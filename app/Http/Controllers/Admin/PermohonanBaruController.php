<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permohonan;
use App\Models\PermohonanStatus;
use App\Models\Status;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PermohonanBaruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['permohonan'] = Permohonan::all();
        return view('pages.admin.permohonan.baru.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Permohonan::with('latest_permohonan_status.status')->orderBy('id_permohonan');


        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('tanggal_permohonan', 'like', '%' . $search['value'] . '%');
                // $query->orWhere('nama_merk_mesin', 'like', '%' . $search['value'] . '%');

            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];
            $d[] = $row->tanggal_permohonan;
            $d[] = $row->nomor_permohonan;
            $d[] = $row->kriteria_permohonan;
            $d[] = $row->jenis_permohonan;
            $d[] = $row->latest_permohonan_status->status->nama_status;
          
            $btn = '<a href="'.url('permohonan/baru/'.$row->id_permohonan.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-eye"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_permohonan.'" data-hapus-nama="'.$row->nomor_permohonan.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['permohonan'] = Permohonan::find($id);
        $data['status'] = Status::all();
        $data['log'] = PermohonanStatus::where('id_permohonan', $id)->orderBy('id_permohonan_status', 'DESC')->first();
        $bln = date('m', strtotime($data['permohonan']->tanggal_permohonan));
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];

        $bln = $bulan[$bln];
        $hari = date("d", strtotime($data['permohonan']->tanggal_permohonan));
        $tahun = date("Y", strtotime($data['permohonan']->tanggal_permohonan));
        $data['tgl'] = "$hari $bln $tahun";
        
        return view('pages.admin.permohonan.baru.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idus = auth()->user()->id_user;
        $input = $this->validate($request, ['id_status' => 'required']);

        DB::beginTransaction();
        try{
            $status = new PermohonanStatus;
            $status->id_permohonan = $id;
            $status->id_status = $input['id_status'];
            $status->id_user = $idus;
            $status->save();

            DB::commit();

            return redirect()->back()->with('success', 'Berhasil mengubah status permohonan!');

        }catch(\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permohonan = Permohonan::find($id);
        $id_user = $permohonan->id_user;
        $path = public_path("storage/dokumen/permohonan_baru/$id_user/$permohonan->nomor_permohonan");
        
        $delete = $permohonan->delete();
        if($delete){
            if(File::exists($path)){
                File::deleteDirectory($path);
                return redirect()->back()->with('success', 'Berhasil menghapus data!');
            }
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data!');
        }
    }
}
