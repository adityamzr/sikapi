<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['status'] = Status::all();
        return view('pages.admin.status.index', $data);
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Status::orderBy('id_status');


        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                // $query->orWhere('id_kategori', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nama_tipe_mesin', 'like', '%' . $search['value'] . '%');

            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data_tmp);
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];
            $d[] = $i++;
            $d[] = $row->nama_status;
            $d[] = $row->keterangan;
          
            $btn = '<a href="'.url('status/'.$row->id_status.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pen"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_status.'" data-hapus-nama="'.$row->nama_status.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.status.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'nama_status' => 'required',
            'keterangan' => 'required'
        ]);

        $status = Status::create($input);

        if($status){
            return redirect('/status')->with('success', 'Berhasil tambah data!');
        }else{
            return redirect('/status/form')->with('error', 'Gagal tambah data!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['status'] = Status::find($id);
        return view('pages.admin.status.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'nama_status' => 'required',
            'keterangan' => 'required'
        ]);

        $status = Status::find($id)->update($input);

        if($status){
            return redirect('/status')->with('success', 'Berhasil mengubah data!');
        }else{
            return redirect('/status/form')->with('error', 'Gagal mengubah data!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Status::find($id)->delete();

        if($delete){
            return redirect()->back()->with('success', 'Berhasil menghapus data!');
        }else{
            return redirect('/status')->with('error', 'Gagal menghapus data!');
        }
    }
}
