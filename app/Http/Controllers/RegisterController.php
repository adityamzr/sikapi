<?php

namespace App\Http\Controllers;

use App\Models\DetailUser;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'nama_user' => 'required',
            'jabatan' => 'required',
            'alamat' => 'required',
            'rtrw' => 'required',
            'provinsi' => 'required',
            'kabkota' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'ktp' => 'required',
            'nomor_telepon' => 'required',
            'email' => 'required',
            'password' => 'required',
            'confirm_password' => 'required'
        ]);

        DB::beginTransaction();
        try{
            if($input['confirm_password'] == $input['password']){
                $password = bcrypt($input['password']);
                
                $user = new User;
                $user->nama_user = $input['nama_user'];
                $user->email = $input['email'];
                $user->password = $password;
                $user->confirm_password = $password;
                $user->role = "user";
                $user->save();

                $detail = new DetailUser;
                $detail->id_user = $user->id_user;
                $detail->nomor_ktp = $input['ktp'];
                $detail->jabatan = $input['jabatan'];
                $detail->alamat = $input['alamat'];
                $detail->rtrw = $input['rtrw'];
                $detail->provinsi = $input['provinsi'];
                $detail->kabkota = $input['kabkota'];
                $detail->kecamatan = $input['kecamatan'];
                $detail->kelurahan = $input['kelurahan'];
                $detail->nomor_telepon = $input['nomor_telepon'];
                $detail->save();

                DB::commit();

                return redirect('/')->with('success', 'Berhasil buat akun, silahkan Login!');
            }

        }catch(\Exception $e){
            // dd($e);
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
