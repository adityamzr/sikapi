<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\DetailPermohonan;
use App\Models\DetailUser;
use App\Models\DokumenPersyaratan;
use App\Models\Galangan;
use App\Models\Kapal;
use App\Models\Permohonan;
use Illuminate\Support\Facades\DB;
use App\Models\TipeMesin;
use App\Models\MerkMesin;
use App\Models\Mesin;
use App\Models\PermohonanStatus;
use App\Models\Siup;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PermohonanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id_user;
        $data['permohonan'] = Permohonan::where('id_user', $user_id)->get();
        return view('pages.user.permohonan.index', $data);
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $id = auth()->user()->id_user;
        $data = Permohonan::with('latest_permohonan_status.status')->where('id_user', $id)->orderBy('id_permohonan');


        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('tanggal_permohonan', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nomor_permohonan', 'like', '%' . $search['value'] . '%');

            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];
            $d[] = $row->tanggal_permohonan;
            $d[] = $row->nomor_permohonan;
            $d[] = $row->kriteria_permohonan;
            $d[] = $row->jenis_permohonan;
            $d[] = $row->latest_permohonan_status->status->nama_status;
          
            $btn = '<a href="'.url('permohonan/baru/'.$row->id_permohonan.'/edit') .'" class="btn btn-primary-light mx-1"><i class="fas fa-eye"></i></a>';

            if($row->latest_permohonan_status->status->id_status == 1){
                $btn .= '<a href="'.url('permohonan/'.$row->id_permohonan.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pen"></i></a>';
                $btn .= '<button class="btn btn-danger btn-hapus mx-1" data-hapus-id="'.$row->id_permohonan.'" data-hapus-nama="'.$row->nomor_permohonan.'"><i class="fas fa-trash"></i></button>';
            }

            if($row->latest_permohonan_status->status->id_status == 8){
                $btn .= '<a href="'.url('permohonan/'.$row->id_permohonan.'/cetak_sk') .'" class="btn btn-danger mx-1"><i class="fas fa-download"></i> SK</a>';
            }

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['tipe_mesin'] = TipeMesin::all();
        $data['merk_mesin'] = MerkMesin::all();
        return view('pages.user.permohonan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $id = auth()->user()->id_user;

        // unset($_COOKIE['pemilik']);

        // return redirect('/permohonan')->with('success', 'Berhasil membuat permohonan!');
            
        $input = $this->validate($request, [
            "kriteria_permohonan" => "required",
            "nomor_permohonan" => "required",
            "jenis_permohonan" => "required",
            "tanggal_permohonan" => "required",
            "nama_pemilik" => "required",
            "nomor_siup" => "required",
            "alamat_pemilik" => "required",
            "email_pemilik" => "required",
            "telepon_pemilik" => "required",
            "alokasi_izin_usaha" => "required",
            "nama_kapal" => "required",
            "tanda_selar" => "required",
            "jenis_alat_tangkap" => "required",
            "ukuran_kapal" => "required",
            "nama_galangan" => "required",
            "provinsi_galangan" => "required",
            "kabkota_galangan" => "required",
            "kecamatan_galangan" => "required",
            "kelurahan_galangan" => "required",
            "email_galangan" => "required",
            "telepon_galangan" => "required",
            "rtrw_galangan" => "required",
            "ktp_galangan" => "required",
            "lati" => "required",
            "longi" => "required",
            "tempat_pembangunan" => "required",
            "tahun_pembangunan" => "required",
            "bahan_kapal" => "required",
            "range_gt" => "required",
            "gt_kapal" => "required",
            "nt_kapal" => "required",
            "loa" => "required",
            "panjang" => "required",
            "lebar" => "required",
            "dalam" => "required",
            "id_merk_mesin" => "required",
            "id_tipe_mesin" => "required",
            "nomor_mesin" => "required",
            "daya_mesin" => "required",
            "jumlah_palka" => "required",
            "kapasitas_palka" => "required",
            "temperatur_palka" => "required",
            "pendingin_palka" => "required",
            "tgl_pembangunan_dari" => "required",
            "tgl_pembangunan_sampai" => "required",
            'file_form_1' => 'required|mimetypes:application/pdf',
            'file_form_2' => 'required|mimetypes:application/pdf',
            "file_tanda_pendaftaran_kapal" => 'required|mimetypes:application/pdf',
            'file_siup' => 'required|mimetypes:application/pdf',
            'file_ktp' => 'required|mimetypes:application/pdf',
            'file_surat_kuasa' => 'required|mimetypes:application/pdf',
            'file_rancang_bangun' => 'required|mimetypes:application/pdf',
            'file_alat_tangkap' => 'required|mimetypes:application/pdf',
            'file_persetujuan' => 'required|mimetypes:application/pdf',
        ],[
            'required' => 'Field Tidak Boleh Kosong!',
            'mimetypes' => 'Format Harus .pdf!'
        ]);

        $tgl_permohonan = date('Y-m-d', strtotime($input['tanggal_permohonan']));   
        $tgl_dari = date('Y-m-d', strtotime($input['tgl_pembangunan_dari']));   
        $tgl_sampai = date('Y-m-d', strtotime($input['tgl_pembangunan_sampai']));   

        DB::beginTransaction();
        try{
            $permohonan = new Permohonan;
            $permohonan->id_user = $id;
            $permohonan->nomor_permohonan =  $input['nomor_permohonan'];
            $permohonan->kriteria_permohonan =  $input['kriteria_permohonan'];
            $permohonan->jenis_permohonan =  $input['jenis_permohonan'];
            $permohonan->tanggal_permohonan =  $tgl_permohonan;
            $permohonan->save(); 
            
            DB::commit();
            
            $data['permohonan'] = Permohonan::find($permohonan->id_permohonan);

            $detail = new DetailPermohonan;
            $detail->id_permohonan = $data['permohonan']->id_permohonan;
            $detail->alokasi_izin_usaha = $input['alokasi_izin_usaha'];
            $detail->jenis_alat_tangkap = $input['jenis_alat_tangkap'];
            $detail->tgl_pembangunan_dari = $tgl_dari;
            $detail->tgl_pembangunan_sampai = $tgl_sampai;
            $detail->save();

            $data['detail_permohonan'] = DetailPermohonan::find($detail->id_detail_permohonan);

            $siup = new Siup;
            $siup->id_detail_permohonan = $data['detail_permohonan']->id_detail_permohonan;
            $siup->nama_pemilik = $input['nama_pemilik'];
            $siup->nomor_siup = $input['nomor_siup'];
            $siup->alamat_pemilik = $input['alamat_pemilik'];
            $siup->email_pemilik = $input['email_pemilik'];
            $siup->telepon_pemilik = $input['telepon_pemilik'];
            $siup->save();

            $kapal = new Kapal;
            $kapal->id_detail_permohonan = $data['detail_permohonan']->id_detail_permohonan;
            $kapal->ukuran_kapal = $input['ukuran_kapal'];
            $kapal->nama_kapal = $input['nama_kapal'];
            $kapal->tanda_selar = $input['tanda_selar'];
            $kapal->tempat_pembangunan = $input['tempat_pembangunan'];
            $kapal->tahun_pembangunan = $input['tahun_pembangunan'];
            $kapal->bahan_kapal = $input['bahan_kapal'];
            $kapal->gt_kapal = $input['gt_kapal'];
            $kapal->nt_kapal = $input['nt_kapal'];
            $kapal->loa = $input['loa'];
            $kapal->panjang = $input['panjang'];
            $kapal->lebar = $input['lebar'];
            $kapal->dalam = $input['dalam'];
            $kapal->jumlah_palka = $input['jumlah_palka'];
            $kapal->kapasitas_palka = $input['kapasitas_palka'];
            $kapal->temperatur_palka = $input['temperatur_palka'];
            $kapal->pendingin_palka = $input['pendingin_palka'];
            $kapal->range_gt = $input['range_gt'];
            $kapal->save();

            $galangan = new Galangan;
            $galangan->id_detail_permohonan = $data['detail_permohonan']->id_detail_permohonan;
            $galangan->nama_galangan = $input['nama_galangan'];
            $galangan->provinsi_galangan = $input['provinsi_galangan'];
            $galangan->kabkota_galangan = $input['kabkota_galangan'];
            $galangan->kecamatan_galangan = $input['kecamatan_galangan'];
            $galangan->kelurahan_galangan = $input['kelurahan_galangan'];
            $galangan->rtrw_galangan = $input['rtrw_galangan'];
            $galangan->email_galangan = $input['email_galangan'];
            $galangan->telepon_galangan = $input['telepon_galangan'];
            $galangan->ktp_galangan = $input['ktp_galangan'];
            $galangan->lati = $input['lati'];
            $galangan->longi = $input['longi'];
            $galangan->save();

            $mesin = new Mesin;
            $mesin->id_detail_permohonan = $data['detail_permohonan']->id_detail_permohonan;
            $mesin->id_tipe_mesin = $input['id_tipe_mesin'];
            $mesin->id_merk_mesin = $input['id_merk_mesin'];
            $mesin->nomor_mesin = $input['nomor_mesin'];
            $mesin->daya_mesin = $input['daya_mesin'];
            $mesin->save();

            $status = new PermohonanStatus;
            $status->id_user = $id;
            $status->id_permohonan = $data['permohonan']->id_permohonan;
            $status->id_status = 1;
            $status->save();
            
            $no_permohonan = $data['permohonan']->nomor_permohonan;

            // dd($no_permohonan);

            foreach($request->files->all() as $key=>$item){
                $filename = $key.'.pdf';
                $dokumen = new DokumenPersyaratan;
                $currentData = DokumenPersyaratan::where('id_permohonan', $dokumen->id_permohonan)->where('jenis_dokumen', $key)->first();
                if($currentData != null){
                    $dokumen = $currentData;
                }
                $dokumen->id_permohonan = $data['permohonan']->id_permohonan;
                $dokumen->nama_dokumen = $filename;
                $dokumen->jenis_dokumen = $key;
                $dokumen->tanggal_upload = date('Y-m-d');
                $dokumen->save();
                $request->file($key)->storeAs("dokumen/permohonan_baru/$id/$no_permohonan", $filename, 'public');
            } 

            
            DB::commit();

            return redirect('/permohonan')->with('success', 'Berhasil membuat permohonan!');

        }catch(\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cetakForm1(Request $request){
        $id = auth()->user()->id_user;
        $data['user'] = User::find($id);
        $data['detail'] = DetailUser::where('id_user', $id)->first();
        $data['cookie'] = [
            "nomor" => $_COOKIE['nomor'],
            "pemilik" => $_COOKIE['pemilik'],
            "siup" => $_COOKIE['siup'],
            "alamat" => $_COOKIE['alamat'],
            "telepon" => $_COOKIE['telepon'],
            "email" => $_COOKIE['email'],
        ];

        $path = public_path('assets/images/LogoBanten.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $img = file_get_contents($path);
        $data['pic'] = 'data:image/' .$type. ';base64,' .base64_encode($img);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pages.user.permohonan.cetakForm1', $data);
        // return $pdf->stream();
        return $pdf->download('Form_1.pdf');

    }
    
    public function cetakForm2(){

        $id = auth()->user()->id_user;
        $data['user'] = User::find($id);
        $data['cookie'] = [
            "nomor" => $_COOKIE['nomor'],
            "nama" => $_COOKIE['nama'],
            "alat" => $_COOKIE['alat'],
            "tempat_pembangunan" => $_COOKIE['tempat_pembangunan'],
            "tahun_pembangunan" => $_COOKIE['tahun_pembangunan'],
            "range_gt" => $_COOKIE['range_gt'],
            "gt" => $_COOKIE['gt'],
            "nt" => $_COOKIE['nt'],
            "loa" => $_COOKIE['loa'],
            "panjang" => $_COOKIE['panjang'],
            "lebar" => $_COOKIE['lebar'],
            "dalam" => $_COOKIE['dalam'],
            "bahan_kapal" => $_COOKIE['bahan_kapal'],
            "merk_mesin" => $_COOKIE['merk_mesin'],
            "tipe_mesin" => $_COOKIE['tipe_mesin'],
            "nomor_mesin" => $_COOKIE['nomor_mesin'],
            "daya_mesin" => $_COOKIE['daya_mesin'],
            "jumlah_palka" => $_COOKIE['jumlah_palka'],
            "kapasitas_palka" => $_COOKIE['kapasitas_palka'],
            "temperatur_palka" => $_COOKIE['temperatur_palka'],
            "pendingin_palka" => $_COOKIE['pendingin_palka'],
            "selar" => $_COOKIE['selar'],
            "nama_galangan" => $_COOKIE['nama_galangan'],
            "provinsi" => $_COOKIE['provinsi'],
            "kako" => $_COOKIE['kako'],
            "kecamatan" => $_COOKIE['kecamatan'],
            "kelurahan" => $_COOKIE['kelurahan'],
            "rtrw" => $_COOKIE['rtrw'],
            "telepon_galangan" => $_COOKIE['telepon_galangan'],
            "email_galangan" => $_COOKIE['email_galangan'],
        ];

        $path = public_path('assets/images/LogoBanten.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $img = file_get_contents($path);
        $data['pic'] = 'data:image/' .$type. ';base64,' .base64_encode($img);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pages.user.permohonan.cetakForm2', $data);
        $pdf = PDF::loadView('pages.user.permohonan.cetakForm2', $data);
        // return $pdf->stream();
        return $pdf->download('Form_2.pdf');
    }

    public function cetakSK($id){
        $data['permohonan'] = Permohonan::with('latest_permohonan_status')->where('id_permohonan', $id)->first();
        $data['detail'] = DetailUser::where('id_user', $data['permohonan']->id_user)->first();
        $data['user'] = User::where('id_user', $data['permohonan']->latest_permohonan_status->id_user)->first();

        $path = public_path('assets/images/LogoBanten.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $img = file_get_contents($path);
        $data['pic'] = 'data:image/' .$type. ';base64,' .base64_encode($img);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pages.user.permohonan.cetakSK', $data);
        // return $pdf->stream();
        return $pdf->download('Surat_Keputusan.pdf');

    }

    public function show($id)
    {
        $data['permohonan'] = Permohonan::find($id);
        $data['log'] = PermohonanStatus::where('id_permohonan', $id)->orderBy('id_permohonan_status', 'DESC')->first();
        $bln = date('m', strtotime($data['permohonan']->tanggal_permohonan));
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];

        $bln = $bulan[$bln];
        $hari = date("d", strtotime($data['permohonan']->tanggal_permohonan));
        $tahun = date("Y", strtotime($data['permohonan']->tanggal_permohonan));
        $data['tgl'] = "$hari $bln $tahun";
        
        return view('pages.admin.permohonan.baru.form', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tipe_mesin'] = TipeMesin::all();
        $data['merk_mesin'] = MerkMesin::all();
        $data['permohonan'] = Permohonan::with('detail_permohonan.siup', 'detail_permohonan.galangan', 'detail_permohonan.kapal', 'detail_permohonan.mesin')->where('id_permohonan', $id)->first();
        // dd($data['permohonan']->detail_permohonan->siup->nama_pemilik); 
        $tanggal = $data['permohonan']->tanggal_permohonan;
        $dari = $data['permohonan']->detail_permohonan->tgl_pembangunan_dari;
        $sampai = $data['permohonan']->detail_permohonan->tgl_pembangunan_sampai;
        $data['dari'] = date('m/d/Y', strtotime($dari));
        $data['sampai'] = date('m/d/Y', strtotime($sampai));
        $data['tanggal'] = date('m/d/Y', strtotime($tanggal));
        return view('pages.user.permohonan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            "kriteria_permohonan" => "required",
            "nomor_permohonan" => "required",
            "jenis_permohonan" => "required",
            "tanggal_permohonan" => "required",
            "nama_pemilik" => "required",
            "nomor_siup" => "required",
            "alamat_pemilik" => "required",
            "email_pemilik" => "required",
            "telepon_pemilik" => "required",
            "alokasi_izin_usaha" => "required",
            "nama_kapal" => "required",
            "tanda_selar" => "required",
            "jenis_alat_tangkap" => "required",
            "ukuran_kapal" => "required",
            "nama_galangan" => "required",
            "provinsi_galangan" => "required",
            "kabkota_galangan" => "required",
            "kecamatan_galangan" => "required",
            "kelurahan_galangan" => "required",
            "email_galangan" => "required",
            "telepon_galangan" => "required",
            "rtrw_galangan" => "required",
            "ktp_galangan" => "required",
            "lati" => "required",
            "longi" => "required",
            "tempat_pembangunan" => "required",
            "tahun_pembangunan" => "required",
            "bahan_kapal" => "required",
            "range_gt" => "required",
            "gt_kapal" => "required",
            "nt_kapal" => "required",
            "loa" => "required",
            "panjang" => "required",
            "lebar" => "required",
            "dalam" => "required",
            "id_merk_mesin" => "required",
            "id_tipe_mesin" => "required",
            "nomor_mesin" => "required",
            "daya_mesin" => "required",
            "jumlah_palka" => "required",
            "kapasitas_palka" => "required",
            "temperatur_palka" => "required",
            "pendingin_palka" => "required",
            "tgl_pembangunan_dari" => "required",
            "tgl_pembangunan_sampai" => "required",
            'file_form_1' => 'required|mimetypes:application/pdf',
            'file_form_2' => 'required|mimetypes:application/pdf',
            "file_tanda_pendaftaran_kapal" => 'required|mimetypes:application/pdf',
            'file_siup' => 'required|mimetypes:application/pdf',
            'file_ktp' => 'required|mimetypes:application/pdf',
            'file_surat_kuasa' => 'required|mimetypes:application/pdf',
            'file_rancang_bangun' => 'required|mimetypes:application/pdf',
            'file_alat_tangkap' => 'required|mimetypes:application/pdf',
            'file_persetujuan' => 'required|mimetypes:application/pdf',
        ],[
            'required' => 'Field Tidak Boleh Kosong!',
            'mimetypes' => 'Format Harus .pdf!'
        ]);
        
        $tgl_permohonan = date('Y-m-d', strtotime($input['tanggal_permohonan']));   
        $tgl_dari = date('Y-m-d', strtotime($input['tgl_pembangunan_dari']));   
        $tgl_sampai = date('Y-m-d', strtotime($input['tgl_pembangunan_sampai']));   

        DB::beginTransaction();
        // try{
            $permohonan = Permohonan::find($id);
            $permohonan->nomor_permohonan =  $input['nomor_permohonan'];
            $permohonan->kriteria_permohonan =  $input['kriteria_permohonan'];
            $permohonan->jenis_permohonan =  $input['jenis_permohonan'];
            $permohonan->tanggal_permohonan =  $tgl_permohonan;
            $permohonan->save(); 
            
            DB::commit();

            $detail = DetailPermohonan::where('id_permohonan', $id)->first();
            $detail->alokasi_izin_usaha = $input['alokasi_izin_usaha'];
            $detail->jenis_alat_tangkap = $input['jenis_alat_tangkap'];
            $detail->tgl_pembangunan_dari = $tgl_dari;
            $detail->tgl_pembangunan_sampai = $tgl_sampai;
            $detail->save();

            $data['detail_permohonan'] = DetailPermohonan::find($detail->id_detail_permohonan);

            $siup = Siup::where('id_detail_permohonan', $data['detail_permohonan']->id_detail_permohonan)->first();
            $siup->nama_pemilik = $input['nama_pemilik'];
            $siup->nomor_siup = $input['nomor_siup'];
            $siup->alamat_pemilik = $input['alamat_pemilik'];
            $siup->email_pemilik = $input['email_pemilik'];
            $siup->telepon_pemilik = $input['telepon_pemilik'];
            $siup->save();

            $kapal = Kapal::where('id_detail_permohonan', $data['detail_permohonan']->id_detail_permohonan)->first();
            $kapal->ukuran_kapal = $input['ukuran_kapal'];
            $kapal->nama_kapal = $input['nama_kapal'];
            $kapal->tanda_selar = $input['tanda_selar'];
            $kapal->tempat_pembangunan = $input['tempat_pembangunan'];
            $kapal->tahun_pembangunan = $input['tahun_pembangunan'];
            $kapal->bahan_kapal = $input['bahan_kapal'];
            $kapal->gt_kapal = $input['gt_kapal'];
            $kapal->nt_kapal = $input['nt_kapal'];
            $kapal->loa = $input['loa'];
            $kapal->panjang = $input['panjang'];
            $kapal->lebar = $input['lebar'];
            $kapal->dalam = $input['dalam'];
            $kapal->jumlah_palka = $input['jumlah_palka'];
            $kapal->kapasitas_palka = $input['kapasitas_palka'];
            $kapal->temperatur_palka = $input['temperatur_palka'];
            $kapal->pendingin_palka = $input['pendingin_palka'];
            $kapal->range_gt = $input['range_gt'];
            $kapal->save();

            $galangan = Galangan::where('id_detail_permohonan', $data['detail_permohonan']->id_detail_permohonan)->first();
            $galangan->nama_galangan = $input['nama_galangan'];
            $galangan->provinsi_galangan = $input['provinsi_galangan'];
            $galangan->kabkota_galangan = $input['kabkota_galangan'];
            $galangan->kecamatan_galangan = $input['kecamatan_galangan'];
            $galangan->kelurahan_galangan = $input['kelurahan_galangan'];
            $galangan->rtrw_galangan = $input['rtrw_galangan'];
            $galangan->email_galangan = $input['email_galangan'];
            $galangan->telepon_galangan = $input['telepon_galangan'];
            $galangan->ktp_galangan = $input['ktp_galangan'];
            $galangan->lati = $input['lati'];
            $galangan->longi = $input['longi'];
            $galangan->save();

            $mesin = Mesin::where('id_detail_permohonan', $data['detail_permohonan']->id_detail_permohonan)->first();
            $mesin->id_tipe_mesin = $input['id_tipe_mesin'];
            $mesin->id_merk_mesin = $input['id_merk_mesin'];
            $mesin->nomor_mesin = $input['nomor_mesin'];
            $mesin->daya_mesin = $input['daya_mesin'];
            $mesin->save();
            
            $no_permohonan = $permohonan->nomor_permohonan;

            // dd($no_permohonan);

            foreach($request->files->all() as $key=>$item){
                $filename = $key.'.pdf';
                $dokumen = DokumenPersyaratan::where('id_permohonan', $id)->first();
                $currentData = DokumenPersyaratan::where('id_permohonan', $dokumen->id_permohonan)->where('jenis_dokumen', $key)->first();
                if($currentData != null){
                    $dokumen = $currentData;
                }
                $dokumen->nama_dokumen = $filename;
                $dokumen->jenis_dokumen = $key;
                $dokumen->tanggal_upload = date('Y-m-d');
                $dokumen->save();
                $request->file($key)->storeAs("dokumen/permohonan_baru/$id/$no_permohonan", $filename, 'public');
            } 

            
            DB::commit();

            return redirect('/permohonan')->with('success', 'Berhasil mengubah data permohonan!');

        // }catch(\Exception $e){
        //     DB::rollBack();
        //     return ["error" => $e->getMessage()];
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id_user = auth()->user()->id_user;
        $permohonan = Permohonan::find($id);
        $path = public_path("storage/dokumen/permohonan_baru/$id_user/$permohonan->nomor_permohonan");
        
        $delete = $permohonan->delete();
        if($delete){
            if(File::exists($path)){
                File::deleteDirectory($path);
                return redirect()->back()->with('success', 'Berhasil menghapus data!');
            }
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data!');
        }
    }
}
