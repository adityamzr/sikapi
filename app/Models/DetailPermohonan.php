<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPermohonan extends Model
{
    use HasFactory;

    protected $table = 't_detail_permohonan';

    protected $fllable = [
        'id_detail_permohonan',
        'id_permohonan',
        'jenis_alat_tangkap',
        'tgl_pembangunan_dari',
        'tgl_pembangunan_sampai',
        'wpp',
        'bkp',
        'tanda_pendafataran_kapal',
        'nama_kapal_sebelumnya',
        'id_transmiter'
    ];
    
    protected $primaryKey = "id_detail_permohonan";

    public $timestamps = false;


    public function galangan(){
        return $this->hasOne(Galangan::class, 'id_detail_permohonan', 'id_detail_permohonan');
    }

    public function kapal(){
        return $this->hasOne(Kapal::class, 'id_detail_permohonan', 'id_detail_permohonan');
    }

    public function mesin(){
        return $this->hasOne(Mesin::class, 'id_detail_permohonan', 'id_detail_permohonan');
    }

    public function siup(){
        return $this->hasOne(Siup::class, 'id_detail_permohonan', 'id_detail_permohonan');
    }
    
}
