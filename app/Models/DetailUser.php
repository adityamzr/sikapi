<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailUser extends Model
{
    use HasFactory;

    protected $table = 't_detail_user';

    protected $fillable = [
        'id_detail_user',
        'id_user',
        'nomor_ktp',
        'jabatan',
        'alamat',
        'rtrw',
        'provinsi',
        'kabkota',
        'kecamatan',
        'kelurahan',
        'nomor_telepon'
    ];

    protected $primaryKey = "id_detail_user";

    public $timestamps = false;
}
