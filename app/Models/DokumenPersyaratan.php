<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DokumenPersyaratan extends Model
{
    use HasFactory;

    protected $table = 't_dokumen_persyaratan';

    protected $fillable = [
        'id_dokumen_persyaratan',
        'id_permohonan',
        'nama_dokumen',
        'tanggal_upload'
    ];

    protected $primaryKey = "id_dokumen_persyaratan";

    public $timestamps = false;
}
