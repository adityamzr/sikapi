<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Galangan extends Model
{
    use HasFactory;

    protected $table = 't_galangan';

    protected $fillable = [
        'id_galangan',
        'id_detail_permohonan',
        'nama_galangan',
        'provinsi_galangan',
        'kab/kota_galangan',
        'kecamatan_galangan',
        'kelurahan_galangan',
        'rt/rw_galangan',
        'email_galangan',
        'telepon_galangan',
        'ktp_galangan'
    ];

    protected $primaryKey = "id_galangan";

    public $timestamps = false;
}
