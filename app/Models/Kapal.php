<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kapal extends Model
{
    use HasFactory;

    protected $table = 't_kapal';

    protected $fillable = [
        'id_kapal',
        'id_detail_permohonan',
        'ukuran_kapal',
        'nama_kapal',
        'tanda_selar',
        'tempat_pembangunan',
        'tahun_pembangunan',
        'bahan_kapal',
        'gt_kapal',
        'nt_kapal',
        'loa',
        'panjang',
        'lebar',
        'dalam',
        'jumlah_palka',
        'kapasitas_palka',
        'temperatur_palka',
        'pendingin_palka',
        'range_gt'
    ];

    protected $primaryKey = "id_kapal";

    public $timestamps = false;
}
