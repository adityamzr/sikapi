<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerkMesin extends Model
{
    use HasFactory;

    protected $table = "t_merk_mesin";

    protected $primaryKey = "id_merk_mesin";

    protected $fillable = [
        'id_merk_mesin',
        'nama_merk_mesin'
    ];

    public $timestamps = false;
}
