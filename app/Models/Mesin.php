<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mesin extends Model
{
    use HasFactory;

    protected $table = 't_mesin';

    protected $fillable = [
        'id_mesin',
        'id_detail_permohonan',
        'nomor_mesin',
        'daya_mesin'
    ];

    protected $primaryKey = "id_mesin";

    public $timestamps = false;

    public function tipe(){
        return $this->hasMany(TipeMesin::class, 'id_tipe_mesin', 'id_tipe_mesin');
    }

    public function merk(){
        return $this->hasMany(MerkMesin::class, 'id_merk_mesin', 'id_merk_mesin');
    }
}
