<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permohonan extends Model
{
    use HasFactory;

    protected $table = 't_permohonan';

    protected $fillable = [
        'id_permohonan',
        'id_user',
        'nomor_permohonan',
        'kriteria_permohonan',
        'jenis_permohonan',
        'tanggal_permohonan'
    ];

    protected $primaryKey = "id_permohonan";

    // protected $dates = "tanggal_permohonan";

    public $timestamps = false;

    public function detail_permohonan(){
        return $this->hasOne(DetailPermohonan::class, 'id_permohonan', 'id_permohonan');
    }

    public function permohonan_status(){
        return $this->hasMany(PermohonanStatus::class, 'id_permohonan', 'id_permohonan');
    }

    public function latest_permohonan_status(){
        return $this->hasOne(PermohonanStatus::class, 'id_permohonan', 'id_permohonan')->latest();
    }

    public function user(){
        return $this->hasOne(User::class, 'id_user', 'id_user');
    }

    public function dokumen(){
        return $this->hasOne(DokumenPersyaratan::class, 'id_permohonan', 'id_permohonan');
    }
}
