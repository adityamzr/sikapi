<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermohonanStatus extends Model
{
    use HasFactory;

    protected $table = 't_permohonan_status';

    protected $fillable = [
        'id_permohonan_status',
        'id_user',
        'id_permohonan',
        'id_status'
    ];

    public function status(){
        return $this->hasOne(Status::class, 'id_status', 'id_status');
    }

    protected $primaryKey = "id_permohonan_status";

}
