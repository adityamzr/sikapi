<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siup extends Model
{
    use HasFactory;

    protected $table = 't_siup';

    protected $primaryKey = "id_isup";

    protected $fillable = [
        'id_siup',
        'id_detail_permohonan',
        'nama_pemilik',
        'nomor_siup',
        'alamat_pemilik',
        'email_pemilik',
        'telepon_pemilik'
    ];

    public $timestamps = false;
}
