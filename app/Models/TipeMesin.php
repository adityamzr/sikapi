<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipeMesin extends Model
{
    use HasFactory;

    protected $table = 't_tipe_mesin';

    protected $primaryKey = 'id_tipe_mesin';

    protected $fillable = [
        'id_tipe_mesin',
        'nama_tipe_mesin'
    ];

    public $timestamps = false;
}
