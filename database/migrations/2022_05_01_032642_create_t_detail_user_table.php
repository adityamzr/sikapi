<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_detail_user', function (Blueprint $table) {
            $table->id('id_detail_user');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id_user')->on('t_users')->onDelete('cascade');
            $table->string('nomor_ktp');
            $table->string('jabatan', 50);
            $table->text('alamat');
            $table->string('rtrw');
            $table->string('provinsi');
            $table->string('kabkota');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('nomor_telepon');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_detail_user');
    }
};
