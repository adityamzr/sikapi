<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_permohonan', function (Blueprint $table) {
            $table->id('id_permohonan');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id_user')->on('t_users')->onDelete('cascade');
            $table->string('nomor_permohonan', 50);
            $table->string('kriteria_permohonan', 30);
            $table->string('jenis_permohonan', 30);
            $table->date('tanggal_permohonan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_permohonan');
    }
};
