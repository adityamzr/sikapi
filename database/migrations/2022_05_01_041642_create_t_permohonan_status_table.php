<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_permohonan_status', function (Blueprint $table) {
            $table->id('id_permohonan_status');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id_user')->on('t_users')->onDelete('cascade');
            $table->unsignedBigInteger('id_permohonan');
            $table->foreign('id_permohonan')->references('id_permohonan')->on('t_permohonan')->onDelete('cascade');
            $table->unsignedBigInteger('id_status');
            $table->foreign('id_status')->references('id_status')->on('t_status')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_permohonan_status');
    }
};
