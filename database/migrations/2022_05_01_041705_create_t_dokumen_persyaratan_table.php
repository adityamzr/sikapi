<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_dokumen_persyaratan', function (Blueprint $table) {
            $table->id('id_dokumen_persyaratan');
            $table->unsignedBigInteger('id_permohonan');
            $table->foreign('id_permohonan')->references('id_permohonan')->on('t_permohonan')->onDelete('cascade');
            $table->string('nama_dokumen', 50);
            $table->string('jenis_dokumen', 50);
            $table->date('tanggal_upload');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_dokumen_persyaratan');
    }
};
