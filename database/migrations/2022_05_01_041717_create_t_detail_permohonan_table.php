<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_detail_permohonan', function (Blueprint $table) {
            $table->id('id_detail_permohonan');
            $table->unsignedBigInteger('id_permohonan');
            $table->foreign('id_permohonan')->references('id_permohonan')->on('t_permohonan')->onDelete('cascade');
            $table->string('alokasi_izin_usaha', 30);
            $table->string('jenis_alat_tangkap');
            $table->date('tgl_pembangunan_dari');
            $table->date('tgl_pembangunan_sampai');
            $table->string('wpp')->nullable();
            $table->string('bkp')->nullable();
            $table->string('tanda_pendaftaran_kapal')->nullable();
            $table->string('nama_kapal_sebelumnya')->nullable();
            $table->bigInteger('id_transmiter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_detail_permohonan');
    }
};
