<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_mesin', function (Blueprint $table) {
            $table->id('id_mesin');
            $table->unsignedBigInteger('id_detail_permohonan');
            $table->foreign('id_detail_permohonan')->references('id_detail_permohonan')->on('t_detail_permohonan')->onDelete('cascade');
            $table->bigInteger('id_merk_mesin');
            $table->bigInteger('id_tipe_mesin');
            $table->string('nomor_mesin', 15);
            $table->string('daya_mesin', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_mesin');
    }
};
