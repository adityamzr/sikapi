<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kapal', function (Blueprint $table) {
            $table->id('id_kapal');
            $table->unsignedBigInteger('id_detail_permohonan');
            $table->foreign('id_detail_permohonan')->references('id_detail_permohonan')->on('t_detail_permohonan')->onDelete('cascade');
            $table->bigInteger('ukuran_kapal');
            $table->string('nama_kapal', 30);
            $table->string('tanda_selar', 50);
            $table->string('tempat_pembangunan', 20);
            $table->bigInteger('tahun_pembangunan');
            $table->string('bahan_kapal', 20);
            $table->bigInteger('gt_kapal');
            $table->bigInteger('nt_kapal');
            $table->bigInteger('loa');
            $table->double('panjang');
            $table->double('lebar');
            $table->double('dalam');
            $table->bigInteger('jumlah_palka');
            $table->bigInteger('kapasitas_palka');
            $table->bigInteger('temperatur_palka');
            $table->string('pendingin_palka', 20);
            $table->string('range_gt', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kapal');
    }
};
