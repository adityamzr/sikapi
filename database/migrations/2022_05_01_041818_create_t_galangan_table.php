<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_galangan', function (Blueprint $table) {
            $table->id('id_galangan');
            $table->unsignedBigInteger('id_detail_permohonan');
            $table->foreign('id_detail_permohonan')->references('id_detail_permohonan')->on('t_detail_permohonan')->onDelete('cascade');
            $table->string('nama_galangan', 100);
            $table->string('provinsi_galangan');
            $table->string('kabkota_galangan');
            $table->string('kecamatan_galangan');
            $table->string('kelurahan_galangan');
            $table->string('rtrw_galangan');
            $table->string('email_galangan');
            $table->string('telepon_galangan', 30);
            $table->string('ktp_galangan', 30);
            $table->string('lati', 100);
            $table->string('longi', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_galangan');
    }
};
