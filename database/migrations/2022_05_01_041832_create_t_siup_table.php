<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_siup', function (Blueprint $table) {
            $table->id('id_siup');
            $table->unsignedBigInteger('id_detail_permohonan');
            $table->foreign('id_detail_permohonan')->references('id_detail_permohonan')->on('t_detail_permohonan')->onDelete('cascade');
            $table->string('nama_pemilik', 30);
            $table->bigInteger('nomor_siup');
            $table->text('alamat_pemilik');
            $table->string('email_pemilik');
            $table->string('telepon_pemilik', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_siup');
    }
};
