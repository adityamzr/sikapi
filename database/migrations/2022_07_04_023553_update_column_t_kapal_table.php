<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_kapal', function (Blueprint $table) {
            $table->dropColumn('loa');
        });

        Schema::table('t_kapal', function (Blueprint $table) {
            $table->double('loa')->after('nt_kapal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_kapal', function (Blueprint $table) {
            $table->dropColumn('loa');
        });

        Schema::table('t_kapal', function (Blueprint $table) {
            $table->bigInteger('loa')->after('nt_kapal');
        });
    }
};
