<?php

namespace Database\Seeders;

use App\Models\MerkMesin;
use App\Models\Status;
use App\Models\TipeMesin;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DefaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert ( [ 
            [
                'nama_user' => 'Super Admin',
                'email' => 'superadmin@admin',
                'password' => bcrypt('superadmin'),
                'confirm_password' => bcrypt('superadmin'),
                'role' => 0
            ],
            [
                'nama_user' => 'Admin',
                'email' => 'admin@admin',
                'password' => bcrypt('admin'),
                'confirm_password' => bcrypt('admin'),
                'role' => 1
            ]
        ]);

        Status::insert([
            [
                'nama_status' => 'PERM',
                'keterangan' => 'PERMOHONAN: Permohonan sudah proses Submit ke Pendok KKP'
            ],
            [
                'nama_status' => 'VER',
                'keterangan' => 'VERIFIKASI: Permohonan dalam proses Verifikasi'
            ],
            [
                'nama_status' => 'LNJ',
                'keterangan' => 'PEMERIKSAAN LANJUTAN: Permohonan dalam proses Pemeriksaan Lanjutan'
            ],
            [
                'nama_status' => 'APPR TOLAK',
                'keterangan' => 'APPROVAL TOLAK: Permohonan dalam proses Approval Penolakan'
            ],
            [
                'nama_status' => 'VU',
                'keterangan' => 'VERIFIKASI ULANG: Permohonan dalam proses Verifikasi Ulang'
            ],
            [
                'nama_status' => 'APPR',
                'keterangan' => 'APPROVAL: Permohonan dalam proses Approval Persetujuan'
            ],
            [
                'nama_status' => 'VAL',
                'keterangan' => 'VALIDASI: Permohonan dalam proses Pengesahan Persetujuan/Penolakan'
            ],
            [
                'nama_status' => 'TERBIT',
                'keterangan' => 'Permohonan disetujui'
            ],
            [
                'nama_status' => 'TOLAK',
                'keterangan' => 'Permohonan ditolak'
            ],
        ]);

        TipeMesin::insert([
            [
                'nama_tipe_mesin' => 'Tipe Mesin A'
            ],
            [
                'nama_tipe_mesin' => 'Tipe Mesin B'
            ]
        ]);

        MerkMesin::insert([
            [
                'nama_merk_mesin' => 'Merk Mesin A'
            ],
            [
                'nama_merk_mesin' => 'Merk Mesin B'
            ]
        ]);

    }
}
