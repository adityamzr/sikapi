// ===============================================================
// FIELDSET ENTRI
// ===============================================================
    
$(document).ready(function(){
    var noper, tanggal, pemilik, siup, alamat, email, telepon;
    
    $('#next_kriteria').on('click',function(){
        if($('#nomor_permohonan').val() == ''){
            let no_permohonan = Math.floor(Math.random() * 90000) + 10000
            nop = "NP-" + no_permohonan
            console.log(nop)
            $('#nomor_permohonan').val(nop)
        }else{
            return
        }
    });
    
    if($('#tanggal_permohonan').val() == ''){
        tanggal = $.cookie('tanggal'); $('#tanggal_permohonan').val(tanggal);
        pemilik = $.cookie('pemilik'); $('#nama_pemilik').val(pemilik);
        siup = $.cookie('siup'); $('#nomor_siup').val(siup);
        alamat = $.cookie('alamat'); $('#alamat_pemilik').val(alamat);
        email = $.cookie('email'); $('#email_pemilik').val(email);
        telepon = $.cookie('telepon'); $('#telepon_pemilik').val(telepon);
    }

    $("#next_entri").on('click', function(e){
        const input = $('#fieldset-entri').find(':input');
        const valid = !Array.from(input.map(function(index, child){
            return child.checkValidity();
        })).includes(false);
        console.log(valid)
        if(valid){
            noper = $('#nomor_permohonan').val(); 
            $.cookie('nomor', noper, { expires: 1, path: '/' });
            tanggal = $('#tanggal_permohonan').val(); 
            $.cookie('tanggal', tanggal, { expires: 1, path: '/' });
            pemilik = $('#nama_pemilik').val(); 
            $.cookie('pemilik', pemilik, { expires: 1, path: '/' });
            siup = $('#nomor_siup').val(); 
            $.cookie('siup', siup, { expires: 1, path: '/' });
            alamat = $('#alamat_pemilik').val(); 
            $.cookie('alamat', alamat, { expires: 1, path: '/' });
            email = $('#email_pemilik').val(); 
            $.cookie('email', email, { expires: 1, path: '/' });
            telepon = $('#telepon_pemilik').val(); 
            $.cookie('telepon', telepon, { expires: 1, path: '/' });
        }else{
            e.preventDefault();
        }
        // let array = [noper, tanggal, pemilik, siup, alamat, email, telepon];
        // console.log(array);

    });

    // ===============================================================
    // FIELDSET PERMOHONAN
    // ===============================================================

    var izin, nama, selar, nakap;

    if($('#alokasi_izin_usaha').val() == ''){
        izin = $.cookie('izin'); $('#alokasi_izin_usaha').val(izin);
        nama = $.cookie('nama'); $('#nama_kapal').val(nama);
        selar = $.cookie('selar'); $('#tanda_selar').val(selar);
    }
    
    $("#next_permohonan").on('click', function(){
        izin = $('#alokasi_izin_usaha').val(); $.cookie('izin', izin, { expires: 1, path: '/' });
        jenis = $('.jenis').val(); $.cookie('jenis', jenis, { expires: 1, path: '/' });
        nama = $('#nama_kapal').val();$.cookie('nama', nama, { expires: 1, path: '/' });
        selar = $('#tanda_selar').val(); $.cookie('selar', selar, { expires: 1, path: '/' });
        
        $('#kapal').val(nama);
        $('#jenis_permohonan').val(jenis);
        // let array = [jenis, izin, nama, selar];
    });

    // ===============================================================
    // FIELDSET CETAK
    // ===============================================================

    var lati, longi, jenis_permohonan, kapal, alat_tangkap, wpp, ukuran_kapal, bkp, nama_kapal_sebelumnya, id_transmiter, nama_galangan, provinsi, kako, kecamatan, kelurahan, email_galangan, telepon_galangan, rtrw, ktp, tempat_pembangunan, tahun_pembangunan, bahan_kapal, range_gt, gt, nt, loa, panjang, lebar, dalam, merk_mesin, tipe_mesin, nomor_mesin, daya_mesin, jumlah_palka, kapasitas_palka, temperatur_palka, pendingin_palka, dari, sampai;

    if($('#alat_tangkap').val() == ''){
        jenis_permohonan = $.cookie('jenis'); $('#jenis_permohonan').val(jenis_permohonan);
        kapal = $('#kapal_hidden').val(); $('#kapal').val(kapal); 
        alat = $.cookie('alat'); $('#alat_tangkap').val(alat);wpp = $.cookie('wpp'); $('#wpp').val(wpp);ukuran_kapal = $.cookie('ukuran_kapal'); $('#ukuran_kapal').val(ukuran_kapal);bkp = $.cookie('bkp'); $('#bkp').val(bkp);nama_kapal_sebelumnya = $.cookie('nama_kapal_sebelumnya'); $('#nama_kapal_sebelumnya').val(nama_kapal_sebelumnya);id_transmiter = $.cookie('id_transmiter'); $('#id_transmiter').val(id_transmiter);nama_galangan = $.cookie('nama_galangan'); $('#nama_galangan').val(nama_galangan);provinsi = $.cookie('provinsi'); $('#provinsi').val(provinsi);kako = $.cookie('kako'); $('#kako').val(kako);kecamatan = $.cookie('kecamatan'); $('#kecamatan').val(kecamatan);kelurahan = $.cookie('kelurahan'); $('#kelurahan').val(kelurahan);email_galangan = $.cookie('email_galangan'); $('#email_galangan').val(email_galangan);telepon_galangan = $.cookie('telepon_galangan'); $('#telepon_galangan').val(telepon_galangan);rtrw = $.cookie('rtrw'); $('#rtrw').val(rtrw);ktp = $.cookie('ktp'); $('#ktp').val(ktp);tempat_pembangunan = $.cookie('tempat_pembangunan'); $('#tempat_pembangunan').val(tempat_pembangunan);tahun_pembangunan = $.cookie('tahun_pembangunan'); $('#tahun_pembangunan').val(tahun_pembangunan);bahan_kapal = $.cookie('bahan_kapal'); $('#bahan_kapal').val(bahan_kapal);range_gt = $.cookie('range_gt'); $('#range_gt').val(range_gt);gt = $.cookie('gt'); $('#gt').val(gt);nt = $.cookie('nt'); $('#nt').val(nt);loa = $.cookie('loa'); $('#loa').val(loa);panjang = $.cookie('panjang'); $('#panjang').val(panjang);lebar = $.cookie('lebar'); $('#lebar').val(lebar);dalam = $.cookie('dalam'); $('#dalam').val(dalam);merk_mesin = $.cookie('merk_mesin'); $('#merk_mesin').val(merk_mesin);tipe_mesin = $.cookie('tipe_mesin'); $('#tipe_mesin').val(tipe_mesin);nomor_mesin = $.cookie('nomor_mesin'); $('#nomor_mesin').val(nomor_mesin);daya_mesin = $.cookie('daya_mesin'); $('#daya_mesin').val(daya_mesin); jumlah_palka = $.cookie('jumlah_palka'); $('#jumlah_palka').val(jumlah_palka); kapasitas_palka = $.cookie('kapasitas_palka'); $('#kapasitas_palka').val(kapasitas_palka); temperatur_palka = $.cookie('temperatur_palka'); $('#temperatur_palka').val(temperatur_palka); pendingin_palka = $.cookie('pendingin_palka'); $('#pendingin_palka').val(pendingin_palka); dari = $.cookie('dari'); $('#dari').val(dari); sampai = $.cookie('sampai'); $('#sampai').val(sampai); lati = $.cookie('lati'); $('#lati').val(lati); longi = $.cookie('longi'); $('#longi').val(longi);
    }

    $("#simpan").on('click', function(){
        // izin = $('#alokasi_izin_usaha').val(); $.cookie('izin', izin, { expires: 1, path: '/' });
        // nama = $('#nama_kapal').val(); $.cookie('nama', nama, { expires: 1, path: '/' });
        lati = $('#lati').val(); longi = $('#longi').val();
        alat_tangkap = $('#alat_tangkap').val(); wpp = $('#wpp').val(); ukuran_kapal = $('#ukuran_kapal').val(); bkp = $('#bkp').val(); nama_kapal_sebelumnya = $('#nama_kapal_sebelumnya').val(); id_transmiter = $('#id_transmiter').val(); nama_galangan = $('#nama_galangan').val(); provinsi = $('#provinsi').val(); kako = $('#kako').val(); kecamatan = $('#kecamatan').val(); kelurahan = $('#kelurahan').val(); email_galangan = $('#email_galangan').val(); telepon_galangan = $('#telepon_galangan').val(); rtrw = $('#rtrw').val(); ktp = $('#ktp').val(); tempat_pembangunan = $('#tempat_pembangunan').val(); tahun_pembangunan = $('#tahun_pembangunan').val(); bahan_kapal = $('#bahan_kapal').val(); range_gt = $('#range_gt').val(); gt = $('#gt').val(); nt = $('#nt').val(); loa = $('#loa').val(); panjang = $('#panjang').val(); lebar = $('#lebar').val(); dalam = $('#dalam').val(); merk_mesin = $('#merk_mesin').val(); tipe_mesin = $('#tipe_mesin').val(); 
        nomor_mesin = $('#nomor_mesin').val(); daya_mesin = $('#daya_mesin').val(); jumlah_palka = $('#jumlah_palka').val(); kapasitas_palka = $('#kapasitas_palka').val(); temperatur_palka = $('#temperatur_palka').val(); pendingin_palka = $('#pendingin_palka').val(); dari = $('#dari').val(); sampai = $('#sampai').val(); 
        
        $.cookie('lati', lati, { expires: 1, path: '/' }); $.cookie('longi', longi, { expires: 1, path: '/' });
        $.cookie('alat', alat_tangkap, { expires: 1, path: '/' }); $.cookie('wpp', wpp, { expires: 1, path: '/' }); $.cookie('ukuran_kapal', ukuran_kapal, { expires: 1, path: '/' }); $.cookie('bkp', bkp, { expires: 1, path: '/' }); $.cookie('nama_kapal_sebelumnya', nama_kapal_sebelumnya, { expires: 1, path: '/' }); $.cookie('id_transmiter', id_transmiter, { expires: 1, path: '/' }); $.cookie('nama_galangan', nama_galangan, { expires: 1, path: '/' }); $.cookie('provinsi', provinsi, { expires: 1, path: '/' }); $.cookie('kako', kako, { expires: 1, path: '/' }); $.cookie('kecamatan', kecamatan, { expires: 1, path: '/' }); $.cookie('kelurahan', kelurahan, { expires: 1, path: '/' }); $.cookie('telepon_galangan', telepon_galangan, { expires: 1, path: '/' }); $.cookie('rtrw', rtrw, { expires: 1, path: '/' }); $.cookie('ktp', ktp, { expires: 1, path: '/' }); $.cookie('tempat_pembangunan', tempat_pembangunan, { expires: 1, path: '/' }); $.cookie('tahun_pembangunan', tahun_pembangunan, { expires: 1, path: '/' }); $.cookie('bahan_kapal', bahan_kapal, { expires: 1, path: '/' }); $.cookie('range_gt', range_gt, { expires: 1, path: '/' }); $.cookie('gt', gt, { expires: 1, path: '/' }); $.cookie('nt', nt, { expires: 1, path: '/' }); $.cookie('loa', loa, { expires: 1, path: '/' }); $.cookie('panjang', panjang, { expires: 1, path: '/' }); $.cookie('lebar', lebar, { expires: 1, path: '/' }); $.cookie('dalam', dalam, { expires: 1, path: '/' }); $.cookie('merk_mesin', merk_mesin, { expires: 1, path: '/' }); $.cookie('tipe_mesin', tipe_mesin, { expires: 1, path: '/' }); $.cookie('nomor_mesin', nomor_mesin, { expires: 1, path: '/' }); $.cookie('daya_mesin', daya_mesin, { expires: 1, path: '/' }); $.cookie('jumlah_palka', jumlah_palka, { expires: 1, path: '/' }); $.cookie('kapasitas_palka', kapasitas_palka, { expires: 1, path: '/' }); $.cookie('temperatur_palka', temperatur_palka, { expires: 1, path: '/' }); $.cookie('pendingin_palka', pendingin_palka, { expires: 1, path: '/' }); $.cookie('dari', dari, { expires: 1, path: '/' }); $.cookie('sampai', sampai, { expires: 1, path: '/' }); $.cookie('email_galangan', email_galangan, { expires: 1, path: '/' });

        console.log($.cookie())
        // let array = [alat_tangkap, wpp, ukuran_kapal, bkp, nama_kapal_sebelumnya, id_transmiter, nama_galangan, provinsi, kako, kecamatan, kelurahan, email_galangan, telepon_galangan, rtrw, ktp, tempat_pembangunan, tahun_pembangunan, bahan_kapal, range_gt, gt, nt, loa, panjang, lebar, dalam, merk_mesin, tipe_mesin, nomor_mesin, daya_mesin, jumlah_palka, kapasitas_palka, temperatur_palka, pendingin_palka, dari, sampai];
        // console.log(array);
        Swal.fire({
            position: 'top-center',
            icon: 'success',
            title: 'Success!',
            text: 'Data Berhasil Disimpan!',
            showConfirmButton: false,
            timer: 1500
        })
    });

    $('#batal1').on('click', function(){
        $('#f1').val('');
    })
    $('#batal2').on('click', function(){
        $('#f2').val('');
    })
    $('#batal3').on('click', function(){
        $('#isiup').val('');
    })
    $('#batal4').on('click', function(){
        $('#sk').val('');
    })
    $('#batal5').on('click', function(){
        $('#ktp').val('');
    })
    $('#batal6').on('click', function(){
        $('#kpl').val('');
    })
    $('#batal7').on('click', function(){
        $('#alat').val('');
    })
    $('#batal8').on('click', function(){
        $('#surat').val('');
    })

    $('#invalidCheck').change(function(){
        console.log($(this)[0]);
        if($(this)[0].checked){
            $('#submit_permohonan').prop('disabled', false);
        }else
            $('#submit_permohonan').prop('disabled', true);
    });

    $('#submit_permohonan').on('click', function(){
        let dataLocal = [
            'nomor', 'tanggal', 'pemilik', 'ukuran_kapal', 'siup', 'alamat', 'email', 'telepon', 'izin', 'nama', 'selar', 'jenis', 'lati', 'longi', 'alat', 'nama_galangan', 'provinsi', 'kako', 'kecamatan', 'kelurahan', 'email_galangan', 'telepon_galangan', 'rtrw', 'ktp', 'tempat_pembangunan', 'tahun_pembangunan', 'bahan_kapal', 'range_gt', 'gt', 'nt', 'loa', 'panjang', 'lebar', 'dalam', 'merk_mesin', 'tipe_mesin', 'nomor_mesin', 'daya_mesin', 'jumlah_palka', 'kapasitas_palka', 'temperatur_palka', 'pendingin_palka', 'dari', 'sampai'
        ];

        dataLocal.forEach(function(data){
            $.removeCookie(data, { path: '/' });
            console.log($.removeCookie(data, { path: '/' }))
        });
        console.log($.cookie());
    });

});