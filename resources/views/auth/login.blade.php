@extends('layouts.app')

@section('body')
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="theme-loader">    
        <div class="loader-p"></div>
    </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <section>
    <div class="container-fluid">
        <div class="row">
        <div class="col-xl-7 order-1"><img class="bg-img-cover bg-center" src="{{ asset('assets/images/bg_login.jpg') }}" alt="looginpage"></div>
        <div class="col-xl-5 p-0">
            <div class="login-card">
            <form class="theme-form login-form needs-validation" action="/" method="POST" novalidate="">
                @csrf
                <h4 class="text-center mb-2">Login</h4>
                <h6 class="text-center">Selamat Datang di Website E-PPKP Banten.</h6>
                <div class="form-group">
                <label>Email</label>
                <div class="input-group"><span class="input-group-text"><i class="icon-email"></i></span>
                    <input class="form-control" type="email" required="" name="email" placeholder="Test@gmail.com" @error('email')
                        is-invalid
                    @enderror value="{{ old('email') }}">
                </div>
                @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                        <input class="form-control" type="password" name="password" required="" placeholder="*********">
                        <div class="show-hide"><span class="show">                         </span></div>
                    </div>
                </div>
                {{-- <div class="form-group">
                <div class="checkbox">
                    <input id="checkbox1" type="checkbox">
                    <label class="text-muted" for="checkbox1">Remember password</label>
                </div><a class="link" href="forget-password.html">Forgot password?</a>
                </div> --}}
                <div class="form-group">
                    <label>Recaptcha</label>
                    {!! htmlFormSnippet() !!}
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block w-100">Sign in</button>
                </div>
                <p>Don't have account?<a class="ms-2" href="/register">Create Account</a></p>
                <div class="my-2 mx-auto">
                    <a href="{{ asset('assets/mekanisme.pdf') }}" target="_blank" class="btn btn-primary-light btn-sm mx-auto"><i class="fas fa-book-open"></i> Mekanisme dan Tata Cara</a>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>
    </section>
    <!-- page-wrapper end-->
    <script>
    (function() {
    'use strict';
    window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
    if (form.checkValidity() === false) {
    event.preventDefault();
    event.stopPropagation();
    }
    form.classList.add('was-validated');
    }, false);
    });
    }, false);
    })();
    </script>
@endsection