@extends('layouts.app')

@section('body')
<div class="loader-wrapper">
    <div class="theme-loader">    
      <div class="loader-p"></div>
    </div>
  </div>
  <!-- Loader ends-->
  <!-- page-wrapper Start-->
  <section>         
    <div class="container-fluid p-0" > 
      <div class="row m-0" style="background-color: rgba(36, 105, 92, 0.1);">
        <div class="col-12 p-0" >    
          <div class="card m-0 py-5" style="background-color: rgba(36, 105, 92, 0.1);">
            <form class="theme-form login-form" action="{{ url('/register') }}" method="POST">
              @csrf
              <h4 class="text-center mb-2">Create your account</h4>
              <h6 class="text-center">Enter your personal details to create account</h6>
              <div class="form-group">
                <label>Nama Pemohon</label>
                <div class="input-group">
                  <input class="form-control" type="text" name="nama_user">
                </div>
              </div>
              <div class="form-group">
                <label>Jabatan</label>
                <div class="input-group">
                  <select class="form-select" name="jabatan" id="">
                    <option value="">--Pilih--</option>
                    <option value="Jabatan1">Jabatan1</option>
                    <option value="Jabatan2">Jabatan2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label>Alamat</label>
                <div class="input-group">
                  <textarea name="alamat" id="" cols="50" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label>RT / RW</label>
                <div class="input-group">
                  <input class="form-control" name="rtrw" type="text">
                </div>
              </div>
              <div class="form-group">
                <label>Provinsi</label>
                <div class="input-group">
                  <select class="form-select" name="provinsi" id="">
                    <option value="">--Pilih--</option>
                    <option value="jabar">Jabar</option>
                    <option value="sulteng">Sulteng</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label>Kabupaten/Kota</label>
                <div class="input-group">
                  <select class="form-select" name="kabkota" id="">
                    <option value="">--Pilih--</option>
                    <option value="kab">Kabupaten</option>
                    <option value="kota">Kota</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label>Kecamatan</label>
                <div class="input-group">
                  <select class="form-select" name="kecamatan" id="">
                    <option value="">--Pilih--</option>
                    <option value="kecamatan1">Kecamatan1</option>
                    <option value="kecamatan2">Kecamatan2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label>Kelurahan</label>
                <div class="input-group">
                  <select class="form-select" name="kelurahan" id="">
                    <option value="">--Pilih--</option>
                    <option value="kelurahan1">Kelurahan1</option>
                    <option value="kelurahan2">Kelurahan2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label>Nomor KTP</label>
                <div class="input-group">
                  <input class="form-control" type="number" name="ktp">
                </div>
              </div>
              <div class="form-group">
                <label>Nomor Telepon</label>
                <div class="input-group">
                  <input class="form-control" type="number" name="nomor_telepon">
                </div>
              </div>
              <div class="form-group">
                <label>Email</label>
                <div class="input-group"><span class="input-group-text"><i class="icon-email"></i></span>
                  <input class="form-control" type="email" name="email" >
                </div>
              </div>
              <div class="form-group">
                <label>Password</label>
                <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                  <input class="form-control" type="password" name="password">
                  <div class="show-hide"><span class="show">                         </span></div>
                </div>
              </div>
              <div class="form-group">
                <label>Confirm Password</label>
                <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                  <input class="form-control" type="password" name="confirm_password">
                  <div class="show-hide"><span class="show">                         </span></div>
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-block w-100" type="submit">Create Account</button>
              </div>
              <p>Already have an account?<a class="ms-2" href="/">Sign in</a></p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection