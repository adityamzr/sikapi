
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="viho admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, viho admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('assets') }}/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('assets') }}/images/favicon.png" type="image/x-icon">
    <title>EPPKP - Banten</title>
    <!-- Google font-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/fontawesome.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/prism.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/date-picker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/datatable-extension.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/style.css">
    <link id="color" rel="stylesheet" href="{{ asset('assets') }}/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/form-wizard.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/custom.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
    {!! ReCaptcha::htmlScriptTagJsApi() !!}
  </head>
  <body>
    <!-- Loader starts-->
    @yield('body')
    <!-- latest jquery-->
    <script src="{{ asset('assets') }}/js/jquery-3.5.1.min.js"></script>
    <!-- feather icon js-->
    <script src="{{ asset('assets') }}/js/icons/feather-icon/feather.min.js"></script>
    <script src="{{ asset('assets') }}/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="{{ asset('assets') }}/js/sidebar-menu.js"></script>
    <script src="{{ asset('assets') }}/js/config.js"></script>
    <!-- Bootstrap js-->
    <script src="{{ asset('assets') }}/js/bootstrap/popper.min.js"></script>
    <script src="{{ asset('assets') }}/js/bootstrap/bootstrap.min.js"></script>
    <!-- Plugins JS start-->
    <script src="{{ asset('assets') }}/js/prism/prism.min.js"></script>
    <script src="{{ asset('assets') }}/js/clipboard/clipboard.min.js"></script>
    <script src="{{ asset('assets') }}/js/custom-card/custom-card.js"></script>
    <script src="{{ asset('assets') }}/js/datepicker/date-picker/datepicker.js"></script>
    <script src="{{ asset('assets') }}/js/datepicker/date-picker/datepicker.en.js"></script>
    <script src="{{ asset('assets') }}/js/datepicker/date-picker/datepicker.custom.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.buttons.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/jszip.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/buttons.colVis.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/pdfmake.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/vfs_fonts.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.autoFill.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.select.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/buttons.html5.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/buttons.print.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.responsive.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.keyTable.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.colReorder.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.fixedHeader.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.rowReorder.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/dataTables.scroller.min.js"></script>
    <script src="{{ asset('assets') }}/js/datatable/datatable-extension/custom.js"></script>
    <script src="{{ asset('assets') }}/js/tooltip-init.js"></script>
    <script src="{{ asset('assets') }}/js/form-wizard.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="{{ asset('assets') }}/js/script.js"></script>
    {{-- <script src="{{ asset('assets') }}/js/theme-customizer/customizer.js"></script> --}}
    <!-- login js-->
    <!-- Plugin used-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>

    @stack('script')

    @if (session('success') && !is_array(session('success')))
      <script>
              Swal.fire({
              position: 'top-center',
              icon: 'success',
              title: 'Success!',
              text: '{{ session('success') }}',
              showConfirmButton: false,
              timer: 1500
              })        
          </script>
      @endif
      
      @if (session('error') && !is_array(session('error')))
      <script>
              Swal.fire({
              position: 'top-center',
              icon: 'error',
              title: 'Peringatan!',
              text: '{{ session('error') }}',
              showConfirmButton: false,
              timer: 1500
              })        
          </script>
      @endif


  </body>
</html>