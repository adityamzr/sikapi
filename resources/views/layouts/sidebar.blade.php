<!-- Page Sidebar Start-->
<header class="main-nav">
  <div class="sidebar-user text-center"><img class="img-90 rounded-circle" src="{{ asset('assets') }}/images/dashboard/1.png" alt="">
    <div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
      <h6 class="mt-3 f-14 f-w-600">{{ auth()->user()->nama_user }}</h6></a>
      @php
          $role = auth()->user()->role;
          if($role == 0){
            $role = "Super Admin";
          }elseif($role == 1){
            $role = "Admin";
          }else{
            $role = "Pemohon";
          }
      @endphp
    <p class="mb-0 font-roboto">{{ $role  }}</p>
  </div>
    <nav>
      <div class="main-navbar">
        <div id="mainnav">
          <ul class="nav-menu custom-scrollbar">
            <li class="back-btn">
              <div class="mobile-back text-end"><span>Back</span></div>
            </li>
            <li><a class="nav-link menu-title link-nav" href="/beranda"><i data-feather="home"></i><span>Beranda</span></a></li>
            @if (auth()->user()->role == 'user')
            <li><a class="nav-link menu-title  link-nav" href="/permohonan"><i data-feather="file-text"></i><span>Permohonan</span></a></li>
            @else
            <li class="mega-menu"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="file-text"></i><span>Permohonan</span></a>
              <div class="mega-menu-container menu-content">
                <div class="container">
                  <div class="row">
                    <div class="col mega-box">
                      <div class="link-section">
                        <div class="submenu-title">
                          <h5><strong>Reguler</strong></h5>
                        </div>
                        <div class="submenu-content opensubmegamenu">
                          <ul>
                            <li><a href="/permohonan/baru">Pembangunan</a></li>
                            {{-- <li><a href="/permohonan/modifikasi">Modifikasi</a></li> --}}
                          </ul>
                        </div>
                      </div>
                    </div>
                    {{-- <div class="col mega-box">
                      <div class="link-section">
                        <div class="submenu-title">
                          <h5><strong>Khusus</strong></h5>
                        </div>
                        <div class="submenu-content opensubmegamenu">
                          <ul>
                            <li><a href="/permohonan/stdh">Sudah Terbit Hubla</a></li>
                            <li><a href="/permohonan/btdh">Belum Terbit Hubla</a></li>
                            <li><a href="/permohonan/spm">Sudah Proses Modifikasi</a></li>
                          </ul>
                        </div>
                      </div>
                    </div> --}}
                  </div>
                </div>
              </div>
            </li>
            <li><a class="nav-link menu-title  link-nav" href="/peta-galangan"><i data-feather="anchor"></i><span>Peta Galangan</span></a></li>
            @endif
            @if (auth()->user()->role == 0)
            <li><a class="nav-link menu-title  link-nav" href="/merk-mesin"><i data-feather="database"></i><span>Merk Mesin</span></a></li>
            <li><a class="nav-link menu-title  link-nav" href="/tipe-mesin"><i data-feather="server"></i><span>Tipe Mesin</span></a></li>
            <li><a class="nav-link menu-title  link-nav" href="/status"><i data-feather="alert-circle"></i><span>Status</span></a></li>
            @endif
            {{-- <li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="anchor"></i><span>Beranda</span></a></li> --}}
          </ul>
        </div>
      </div>
    </nav>
  </header>