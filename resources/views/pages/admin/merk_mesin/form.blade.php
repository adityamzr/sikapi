@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Merk Mesin</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/merk-mesin">Daftar</a></li>
            <li class="breadcrumb-item">Form Tambah</li>
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row starter-main">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
          </div>
          <div class="card-body pt-2">
            <form action="{{ url('/merk-mesin', @$merk_mesin->id_merk_mesin) }}" method="POST">
                @csrf
                @if (!empty($merk_mesin))
                    @method('PATCH')
                @endif
                <div class="row g-3">
                    <div class="col-8 offset-1">
                        <label class="form-label" for="">Nama Merk Mesin</label>
                        <input class="form-control" name="nama_merk_mesin" value="{{ old('nama_merk_mesin', @$merk_mesin->nama_merk_mesin) }}" type="text">
                    </div>
                    <div class="col-3 d-flex align-items-end">
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
           
          </div>
        </div>
      </div>
    </div>
</div>
@endsection