@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Detail Permohonan</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data</a></li>
            <li class="breadcrumb-item">Detail Permohonan</li>
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row starter-main">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
          </div>
          <div class="card-body pt-2">
            {{-- STATUS PERMOHONAN --}}
            <div class="row">
              <div class="col-lg-9 d-flex align-items-center">
                <h5 style="margin: 0px;">Status Permohonan</h5>
              </div>
              {{-- @if (@$log->status->id_status == 8 && auth()->user()->role == 'user')
              <div class="col-lg-3 text-end">
                <button class="btn btn-danger">Download SK</button>
              </div>
              @endif --}}
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-4">
                <span>Status Saat Ini</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-4">
                <span>{{ $log->status->nama_status }}</span>
              </div>
            </div>
            <div class="row">
              <div class="col-4">
                <span>Keterangan</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-7">
                <span>{{ $log->status->keterangan }}</span>
              </div>
            </div>
            @if (auth()->user()->role != 'user')
            <form action="{{ url('/permohonan/baru', @$permohonan->id_permohonan) }}" method="POST">
              @csrf
              @if (!empty($permohonan))
              @method('PATCH')
              @endif
            <div class="row my-3">
              <div class="col-4 d-flex align-items-center">
                <span>Ubah Status</span>
              </div>
              <div class="col-1 d-flex align-items-center">
                <span>:</span>
              </div>
                <div class="col-5">
                  <select class="form-select" id="" name="id_status" required>
                      <option value="">--Pilih--</option>
                      @foreach ($status as $item)
                      <option value="{{ $item->id_status }}" {{ old('id_status', @$item->id_status) == @$log->status->id_status ? 'selected' : '' }}>{{ $item->nama_status }}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-2">
                  <button class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </form>
            <div class="accordion accordion-flush" id="accordionFlushExample">
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                  <button class="accordion-button collapsed pr-2" style="padding-left:0px;" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                      Bantuan
                  </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                      <table class="tb-spacing f12">
                          <tbody>
                              <tr>
                                  <td><span class="badge badge-primary">PERM</span></td>
                                  <td colspan="4"><strong>PERMOHONAN:</strong> Permohonan sudah proses Submit ke pendok KKP</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-warning">VER</span></td>
                                  <td colspan="4"><strong>VERIFIKASI:</strong> Permohonan dalam proses Verifikasi</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-warning">LNJ</span></td>
                                  <td colspan="5"><strong>PEMERIKSAAN LANJUTAN:</strong> Permohonan dalam proses Pemeriksaan Lanjutan</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-warning">VU</span></td>
                                  <td colspan="4"><strong>VERIFIKASI ULANG:</strong> Permohonan dalam proses Verifikasi Ulang</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-danger">APPR TOLAK</span></td>
                                  <td colspan="4"><strong>APPROVAL TOLAK:</strong> Permohonan dalam proses Approval Penolaka</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-primary">APPR</span></td>
                                  <td colspan="4"><strong>APPROVAL:</strong> Permohonan dalam proses Approval Persetujuan</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-primary">VAL</span></td>
                                  <td colspan="6"><strong>VALIDASI:</strong> Permohonan dalam proses Pengesahan Persetujuan/Penolakan</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-primary">TERBIT</span></td>
                                  <td colspan="2">Permohonan disetujui</td>
                              </tr>
                              <tr>
                                  <td><span class="badge badge-danger">TOLAK</span></td>
                                  <td colspan="2">Permohonan ditolak</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
          {{-- END CARD BODY --}}
        </div>
      </div>
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
          </div>
          <div class="card-body pt-2">
            {{-- DATA PEMOHON --}}
            <h5>Data Pemohon</h5>
            <hr>
            <div class="row mb-3">
              <div class="col-4">
                <span>Nomor Permohonan</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-4">
                <span>{{ $permohonan->nomor_permohonan }}</span>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-4">
                <span>Tanggal Permohonan</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-4">
                <span>{{ $tgl }}</span>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-4">
                <span>Nama Pemohon</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-4">
                <span>{{ $permohonan->user->nama_user }}</span>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-4">
                <span>Kriteria Permohonan</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-4">
                <span>{{ $permohonan->kriteria_permohonan }}</span>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-4">
                <span>Jenis Permohonan</span>
              </div>
              <div class="col-1">
                <span>:</span>
              </div>
              <div class="col-4">
                <span>{{ $permohonan->jenis_permohonan }}</span>
              </div>
            </div>
            <hr>
            <h5>Dokumen-dokumen</h5>
            <hr>
            {{-- row start --}}
            <div class="row">
              <div class="col-md-6 my-2">
                <span>Form 1</span>
              </div>
              <div class="col-md-6 my-2">
                <span>Form 2</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_form_1.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_form_2.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
            </div>
            {{-- end of row --}}
            {{-- row start --}}
            <div class="row">
              <div class="col-md-6 my-2">
                <span>Tanda Pendaftaran Kapal</span>
              </div>
              <div class="col-md-6 my-2">
                <span>Scan Asli Surat Izin Usaha Perikanan (SIUP)</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_tanda_pendaftaran_kapal.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_siup.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
            </div>
            {{-- end of row --}}
            {{-- row start --}}
            <div class="row">
              <div class="col-md-6 my-2">
                <span>Surat Kuasa</span>
              </div>
              <div class="col-md-6 my-2">
                <span>Scan Asli Kartu Tanda Penduduk (KTP) Pemilik Kapal/Penanggung Jawab Perusahaan</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_surat_kuasa.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_ktp.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
            </div>
            {{-- end of row --}}
            {{-- row start --}}
            <div class="row">
              <div class="col-md-6 my-2">
                <span>Scan Asli Gambar Rancang Bangun Kapal</span>
              </div>
              <div class="col-md-6 my-2">
                <span>Spesifikasi Teknis Alat Tangkap Ikan</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_rancang_bangun.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_alat_tangkap.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
            </div>
            {{-- end of row --}}
            {{-- row start --}}
            <div class="row">
              <div class="col-md-6 my-2">
                <span>Surat Persetujuan Penggunaan Nama Kapal Dari Direktorat Jendral Perhubungan Laut, Kementrian Perhubungan</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-3">
                <a href="{{ asset('storage/dokumen/permohonan_baru/'.$permohonan->id_user.'/'.$permohonan->nomor_permohonan.'/file_persetujuan.pdf') }}" target="_blank" class="btn btn-secondary">Lihat Dokumen</a>
              </div>
            </div>
            {{-- end of row --}}
          </div>
        </div>
      </div>
    </div>
</div>
@endsection