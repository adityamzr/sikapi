@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Permohonan</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Reguler</a></li>
            <li class="breadcrumb-item">Baru</li>
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
  <!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row starter-main">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
            <div class="row">
                <div class="col pull-left">
                    <h5>Pencarian</h5>
                </div>
            </div>
          </div>
          <hr>
          <div class="card-body pt-2">
              <div class="dt-ext table-responsive mt-3">
                <table class="display f12" id="tabelBaru">
                  <thead>
                    <tr>
                      <th>Tgl Permohonan</th>
                      <th>Nomor Permohonan</th>
                      <th>Kriteria</th>
                      <th>Jenis</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
      {{-- Accordion Keterangan --}}
      <div class="col sm-12">
        <div class="card">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                        Keterangan
                    </button>
                  </h2>
                  <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">
                        <table class="tb-spacing f12">
                            <tbody>
                                <tr>
                                    <td><span class="badge badge-primary">BARU</span></td>
                                    <td>Permohonan Baru</td>
                                    <td style="width: 20%"></td>
                                    <td><span class="badge badge-warning">MODIF</span></td>
                                    <td>Permohonan Modifikasi</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">STD HUBLA</span></td>
                                    <td colspan="2">Sudah Bangun,<br>Sudah Terbit Dokumen Hubla</td>
                                    <td><span class="badge badge-primary">BTD HUBLA</span></td>
                                    <td>Sudah Bangun,<br>Belum Terbit Dokumen Hubla</td>
                                    <td><span class="badge badge-primary">SP MODIF</span></td>
                                    <td>Sudah Bangun,<br>Proses/Sudah Modifikasi</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">ENTRI</span></td>
                                    <td colspan="4">Permohonan dalam proses pengisian oleh pemohon</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">PERM</span></td>
                                    <td colspan="4"><strong>PERMOHONAN:</strong> Permohonan sudah proses Submit ke pendok KKP</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">VER</span></td>
                                    <td colspan="4"><strong>VERIFIKASI:</strong> Permohonan dalam proses Verifikasi</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">LNJ</span></td>
                                    <td colspan="5"><strong>PEMERIKSAAN LANJUTAN:</strong> Permohonan dalam proses Pemeriksaan Lanjutan</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">VU</span></td>
                                    <td colspan="4"><strong>VERIFIKASI ULANG:</strong> Permohonan dalam proses Verifikasi Ulang</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-danger">APPR TOLAK</span></td>
                                    <td colspan="4"><strong>APPROVAL TOLAK:</strong> Permohonan dalam proses Approval Penolaka</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">APPR</span></td>
                                    <td colspan="4"><strong>APPROVAL:</strong> Permohonan dalam proses Approval Persetujuan</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">VAL</span></td>
                                    <td colspan="6"><strong>VALIDASI:</strong> Permohonan dalam proses Pengesahan Persetujuan/Penolakan</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">TERBIT</span></td>
                                    <td colspan="2">Permohonan disetujui</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-danger">TOLAK</span></td>
                                    <td colspan="2">Permohonan ditolak</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
</div>
  <!-- Container-fluid Ends-->
@endsection

@push('script')
<script>
    $( document ).ready(function() {

        var table;
        table = $('#tabelBaru').DataTable({
            deferRender: true,
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: {
                url: '{!! route('permohonanBaru.get') !!}',
                type: 'POST',
                data: function (e) {
                    e._token = '{{ csrf_token() }}';
                }
            },
            drawCallback: function (){
                // feather.replace();
            }
        });
    });

    $('body').on('click', '.btn-hapus', function(event){
      event.preventDefault();
      var id = $(this).data('hapus-id');
      var nama = $(this).data('hapus-nama');

      Swal.fire({
        title: 'Peringatan!',
        text: 'Hapus Permohonan '+nama+'?',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Hapus',
    }).then((result) => {
        if (result.isConfirmed) {
          console.log(id);
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "baru/"+id,
            type: 'POST',
            data: {
              "id": id,
              "_token": "{{ csrf_token() }}"
            },
            success: function(data){
              Swal.fire({
              position: 'top-center',
              icon: 'success',
              title: 'Success!',
              text: 'Berhasil menghapus data!',
              showConfirmButton: false,
              timer: 1500
              })
            }
        });
        } else if (result.isDenied) {
          Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
  })
</script>
@endpush