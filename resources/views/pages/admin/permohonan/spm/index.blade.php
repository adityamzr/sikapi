@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Permohonan</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../ltr/index.html">Khusus</a></li>
            <li class="breadcrumb-item">Sudah Proses Modifikasi</li>
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
  <!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row starter-main">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
            <div class="row">
                <div class="col pull-left">
                    <h5>Pencarian</h5>
                </div>
                <div class="col pull-right text-end">
                    <a href="/permohonan/form" class="btn btn-primary" type="submit">+ Tambah</a>
                </div>
            </div>
          </div>
          <hr>
          <div class="card-body pt-2">
            {{-- <form>
                <div class="row g-3">
                    <div class="form-group col-md-3">
                        <label class="col form-label">Tanggal Permohonan</label>
                        <div class="col d-flex">
                            <div class="col-5">
                                <div class="input-group">
                                    <input class="datepicker-here form-control form-control-sm digits" type="text" data-language="en" placeholder="Dari">
                                </div>
                            </div>
                            <div class="col-2">
                                <pre class="w-100 h-100 p-0 m-0 d-flex justify-content-center align-items-center"><i class="" data-feather="more-horizontal"></i></pre>
                            </div>
                            <div class="col-5">
                                <div class="input-group">
                                    <input class="datepicker-here form-control form-control-sm digits" type="text" data-language="en" placeholder="Sampai">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" for="">Nama Kapal</label>
                        <input class="form-control form-control-sm" id="" type="text">
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" for="">Perusahaan</label>
                        <input class="form-control form-control-sm" id="" type="text">
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" for="">Alat Tangkap</label>
                        <select class="form-select" id="">
                            <option>Jaring 1</option>
                            <option>Jaring 2</option>
                            <option>Jaring 3</option>
                        </select>
                        </div>
                    </div>
            </form> --}}
            {{-- <table class="table tb-hover bg-secondary f12">
                <thead>
                  <tr>
                    <th scope="col">Tgl Permohonan</th>
                    <th scope="col">Perusahaan</th>
                    <th scope="col">Nama Kapal</th>
                    <th scope="col">GT</th>
                    <th scope="col">Alat Tangkap</th>
                    <th scope="col">Jenis Permohonan</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table> --}}
              <div class="dt-ext table-responsive mt-3">
                <table class="display f12" id="responsive">
                  <thead>
                    <tr>
                      <th>Tgl Permohonan</th>
                      <th>Perusahaan</th>
                      <th>Nama Kapal</th>
                      <th>GT</th>
                      <th>Alat Tangkap</th>
                      <th>Jenis Permohonan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>2011/04/25</td>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>$320,800</td>
                      <td>61</td>
                      <td>61</td>
                      <td>61</td>
                    </tr>
                    <tr>
                      <td>2011/07/25</td>
                      <td>Garrett Winters</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>$170,750</td>
                      <td>63</td>
                      <td>63</td>
                      <td>63</td>
                    </tr>
                    <tr>
                      <td>2009/01/12</td>
                      <td>Ashton Cox</td>
                      <td>Junior Technical Author</td>
                      <td>San Francisco</td>
                      <td>$86,000</td>
                      <td>66</td>
                      <td>66</td>
                      <td>66</td>
                    </tr>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
      {{-- Accordion Keterangan --}}
      <div class="col sm-12">
        <div class="card">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                        Keterangan
                    </button>
                  </h2>
                  <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">
                        <table class="tb-spacing f12">
                            <tbody>
                                <tr>
                                    <td><span class="badge badge-primary">BARU</span></td>
                                    <td>Permohonan Baru</td>
                                    <td style="width: 20%"></td>
                                    <td><span class="badge badge-warning">MODIF</span></td>
                                    <td>Permohonan Modifikasi</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">STD HUBLA</span></td>
                                    <td colspan="2">Sudah Bangun,<br>Sudah Terbit Dokumen Hubla</td>
                                    <td><span class="badge badge-primary">BTD HUBLA</span></td>
                                    <td>Sudah Bangun,<br>Belum Terbit Dokumen Hubla</td>
                                    <td><span class="badge badge-primary">SP MODIF</span></td>
                                    <td>Sudah Bangun,<br>Proses/Sudah Modifikasi</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">ENTRI</span></td>
                                    <td colspan="4">Permohonan dalam proses pengisian oleh pemohon</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">PERM</span></td>
                                    <td colspan="4"><strong>PERMOHONAN:</strong> Permohonan sudah proses Submit ke pendok KKP</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">VER</span></td>
                                    <td colspan="4"><strong>VERIFIKASI:</strong> Permohonan dalam proses Verifikasi</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">LNJ</span></td>
                                    <td colspan="5"><strong>PEMERIKSAAN LANJUTAN:</strong> Permohonan dalam proses Pemeriksaan Lanjutan</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">VU</span></td>
                                    <td colspan="4"><strong>VERIFIKASI ULANG:</strong> Permohonan dalam proses Verifikasi Ulang</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-danger">APPR TOLAK</span></td>
                                    <td colspan="4"><strong>APPROVAL TOLAK:</strong> Permohonan dalam proses Approval Penolaka</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">APPR</span></td>
                                    <td colspan="4"><strong>APPROVAL:</strong> Permohonan dalam proses Approval Persetujuan</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">VAL</span></td>
                                    <td colspan="6"><strong>VALIDASI:</strong> Permohonan dalam proses Pengesahan Persetujuan/Penolakan</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-primary">TERBIT</span></td>
                                    <td colspan="2">Permohonan disetujui</td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-danger">TOLAK</span></td>
                                    <td colspan="2">Permohonan ditolak</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
</div>
  <!-- Container-fluid Ends-->
@endsection