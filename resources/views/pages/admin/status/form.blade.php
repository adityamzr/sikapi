@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Tipe Mesin</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/status">Daftar</a></li>
            <li class="breadcrumb-item">Form Tambah</li>
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row starter-main">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
          </div>
          <div class="card-body pt-2">
            <form action="{{ url('/status', @$status->id_status) }}" method="POST">
                @csrf
                @if (!empty($status))
                    @method('PATCH')
                @endif
                <div class="row g-3">
                    <div class="col-6">
                        <label class="form-label" for="">Nama Status</label>
                        <input class="form-control" name="nama_status" value="{{ old('nama_status', @$status->nama_status) }}" type="text">
                    </div>
                    <div class="col-6">
                        <label class="form-label" for="">Keterangan</label>
                        <input class="form-control" name="keterangan" value="{{ old('keterangan', @$status->keterangan) }}" type="text">
                    </div>
                </div>
                <div class="row g-3 mt-3 justify-content-end">
                    <div class="col-3 mt-0 text-end">
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection