@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Tipe Mesin</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/tipe-mesin">Daftar</a></li>
            <li class="breadcrumb-item">Form Tambah</li>
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row starter-main">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header pb-0">
          </div>
          <div class="card-body pt-2">
            <form action="{{ url('/tipe-mesin', @$tipe_mesin->id_tipe_mesin) }}" method="POST">
                @csrf
                @if (!empty($tipe_mesin))
                    @method('PATCH')
                @endif
                <div class="row g-3">
                    <div class="col-8 offset-1">
                        <label class="form-label" for="">Nama Tipe Mesin</label>
                        <input class="form-control" name="nama_tipe_mesin" value="{{ old('nama_tipe_mesin', @$tipe_mesin->nama_tipe_mesin) }}" type="text">
                    </div>
                    <div class="col-3 d-flex align-items-end">
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
           
          </div>
        </div>
      </div>
    </div>
</div>
@endsection