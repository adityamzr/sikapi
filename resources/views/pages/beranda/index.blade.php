@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Beranda</h3>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row starter-main">
      {{-- Card Information Start --}}
      <div class="col-sm-6 col-xl-4 col-lg-4">
        <div class="card o-hidden border-0">
          <div class="bg-primary b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="database"></i></div>
              @if (auth()->user()->role == 'user')
              <div class="media-body"><span class="m-0">Permohonan<br>Diterima</span>
                <h4 class="mb-0 counter">{{ $status->where('id_status', 8)->count() }}</h4><i class="icon-bg" data-feather="database"></i>
              </div>
              @else
              <div class="media-body"><span class="m-0">Permohonan<br>Masuk</span>
                <h4 class="mb-0 counter">{{ $permohonan->count() }}</h4><i class="icon-bg" data-feather="database"></i>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-4 col-lg-4">
        <div class="card o-hidden border-0">
          <div class="bg-secondary b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
              @if (auth()->user()->role == 'user')
              <div class="media-body"><span class="m-0">Permohonan<br>Ditolak</span>
                <h4 class="mb-0 counter">{{ $status->where('id_status', 9)->count() }}</h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
              @else
              <div class="media-body"><span class="m-0">Permohonan<br>Diterima</span>
                <h4 class="mb-0 counter">{{ $status->where('id_status', 8)->count() }}</h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-4 col-lg-4">
        <div class="card o-hidden border-0">
          <div class="bg-primary b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="database"></i></div>
              @if (auth()->user()->role == 'user')
              <div class="media-body"><span class="m-0">Permohonan<br>Diproses</span>
                <h4 class="mb-0 counter">{{ $status->where('id_status', 3)->count() }}</h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
              @else
              <div class="media-body"><span class="m-0">Permohonan<br>Ditolak</span>
                <h4 class="mb-0 counter">{{ $status->where('id_status', 9)->count() }}</h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
      {{-- Card Information End --}}
      {{-- Table --}}
      {{-- <form action="/beranda" method="POST">
        @csrf
        <input type="text" name="tes" id="tes">
        <input type="text" name="tos" id="tos">
        <button class="btn btn-primary" type="submit" id="save">simpan cookie</button>
      </form> --}}
      <div class="col-xl-12 recent-order-sec">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <h5>Daftar Permohonan Terbaru</h5>
              <table class="table table-bordernone">                                         
                <thead>
                  <tr>                                        
                    <th>Nama Pemohon</th>
                    <th>Tanggal</th>
                    <th>Kriteria</th>
                    <th>Jenis</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($permohonan as $item)
                  <tr>
                    <td>
                        <a href="#"><span>{{ $item->user->nama_user }}</span></a>
                    </td>
                    <td>
                      <p>{{ $item->tanggal_permohonan }}</p>
                    </td>
                    <td>
                      <p>{{ $item->kriteria_permohonan }}</p>
                    </td>
                    <td>
                      <p>{{ $item->jenis_permohonan }}</p>
                    </td>
                    <td>
                      <p>{{ $item->latest_permohonan_status->status->nama_status }}</p>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
@endsection

{{-- @push('script')
<script>
  var tes, tos;
  tes = $.cookie('tes');
  tos = $.cookie('tos');
  console.log(tes)
  console.log(tos)
  $('#tes').val(tes);
  $('#tos').val(tos);
  $('#save').on('click', function(){
    tes = $('#tes').val(); 
    tos = $('#tos').val(); 
    $.cookie('tes', tes, { expires: 1 });
    $.cookie('tos', tos, { expires: 1 });
  });
</script>
@endpush --}}