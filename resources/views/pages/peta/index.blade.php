@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3>Peta Galangan</h3>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Peta Galangan</a></li>
            {{-- <li class="breadcrumb-item">Peta Galangan</li> --}}
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row starter-main">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header pb-0">
        </div>
        <div class="card-body pt-2">
          <div id="map" class="col-12" style="height: 400px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script>
  var lokasi = {!! json_encode($permohonan->toArray()) !!};
	var map = L.map('map').setView([-6.118366, 106.152306], 13);
	var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(map);

  console.log(lokasi);
  lokasi.forEach(function(peta){
      
      var marker = L.marker([peta.detail_permohonan.galangan.lati, peta.detail_permohonan.galangan.longi]).addTo(map)
      .bindPopup('<b>Lokasi Galangan '+peta.detail_permohonan.galangan.nama_galangan+' Milik</b><br /> <a href="/permohonan/baru/'+peta.id_permohonan+'/edit" target="_blank">'+ peta.detail_permohonan.siup.nama_pemilik + '</a>'  ).openPopup();
  });
</script>
@endpush