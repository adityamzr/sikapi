<div class="row">
    <div class="col-7">
        <h2 class="fs-title">Dokumen Persyaratan</h2>
    </div>
    <div class="col-5">
        <h2 class="steps">Step 5 - 6</h2>
    </div>
</div>
<div class="col-12" id="file">
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="col-md-4 mb-3">
                <span><strong>Form 1</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="f1" type="file" name="file_form_1" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal1" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="col-md-4 mb-3">
                <span><strong>Form 2</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="f2" type="file" name="file_form_2" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal2" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="col-12 mb-3">
                <span><strong>Scan asli surat izin usaha perikanan SIUP</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="isiup" type="file" name="file_siup" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal3" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="col-md-4 mb-3">
                <span><strong>Surat Kuasa</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="sk" type="file" name="file_surat_kuasa" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal4" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="col-12 mb-3">
                <span><strong>Scan asli KTP (Kartu Tanda Penduduk) pemilik kapal/penanggung jawab perusahaan</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="ktp" type="file" name="file_ktp" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal5" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="col-12 mb-3">
                <span><strong>Scan asli gambar rancang bangun kapal</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="kpl" type="file" name="file_rancang_bangun" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal6" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="col-12 mb-3">
                <span><strong>Spesifikasi teknis alat penangkapan ikan yang akan digunakan sesuai dengan jenis dan ukuran utama alat penangkapan ikan sebagaimana tercantum pada Form 4, untuk kapal penangkapan ikan</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="alat" type="file" name="file_alat_tangkap" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal7" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="col-12 mb-3">
                <span><strong>Surat persetujuan penggunaan nama kapal dari Direktorat Jendral Perhubungan Laut, Kementrian Perhubungan</strong></span>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control my-0" id="surat" type="file" name="file_persetujuan" required>
                    <div class="invalid-feedback">
                        File tidak boleh kosong!
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="batal8" class="btn btn-danger w-100"><i class="fa-solid fa-xmark"></i> Batal</button>
                </div>
            </div>
            <div class="col mt-2">
                <span  class="f12" style="font-style: italic">*) Format File PDF, Maks. Size 2MB.</span>
            </div>
        </div>
    </div>
</div>