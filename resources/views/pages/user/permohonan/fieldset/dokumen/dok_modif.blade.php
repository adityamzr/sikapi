
        <div class="row">
            <div class="col-7">
                <h2 class="fs-title">Dokumen Persyaratan</h2>
            </div>
            <div class="col-5">
                <h2 class="steps">Step 5 - 6</h2>
            </div>
        </div>
        <div class="col-12">
            <div class="card" style="border: 1px solid black">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Form 1</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Form 2</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Scan asli surat izin usaha perikanan SIUP</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Surat Kuasa</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Scan asli KTP (Kartu Tanda Penduduk) pemilik kapal/penanggung jawab perusahaan</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_1">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Scan asli gambar rancang bangun kapal rencana/setelah modifikasi</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_2">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Scan asli dokumen kapal meliputi Grosse Akta</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Scan asli surat ukur</strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_4">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Surat persetujuan penggunaan nama kapal atau persetujuan penggantian nama kapal dari Direktorat Jendral Perhubungan Laut, Kementrian Perhubungan, apabila ada penggantian nama kapal
                            </strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_5">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Scan asli Surat izin penangkapan ikan atau surat izin kapal pengangkut ikan SIPI/SIKPI, untuk kapal yang pernah memiliki surat izin penangkapan ikan atau surat izin kapal pengangkut ikan SIPI/SIKPI
                            </strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_6">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Desain dan spesifikasi teknis jenis alat penangkap ikan
                            </strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_7">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Foto kapal sebelum modifikasi (1.Tampak Depan, 2.Tampak Belakang, 3.Tampak Samping Kanan, 4.Tampak Samping Kiri)
                            </strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="border: 1px solid black" id="hidden_dok_modif_8">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <span class="f12"><strong>Buku Kapal Perikanan bagi kapal yang telah terdaftar sebagai kapal perikanan
                            </strong></span>
                        </div>
                        <div class="col-md-8 text-end">
                            <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                            <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                            <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                            <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="bg-secondary">
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>