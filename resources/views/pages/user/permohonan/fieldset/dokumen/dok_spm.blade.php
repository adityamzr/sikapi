<div class="row">
    <div class="col-7">
        <h2 class="fs-title">Dokumen Persyaratan</h2>
    </div>
    <div class="col-5">
        <h2 class="steps">Step 5 - 6</h2>
    </div>
</div>
<div class="col-12">
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Form 1</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Form 2</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Surat Kuasa</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>SIUP</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Kartu Tanda Penduduk pemilik kapal/penanggung jawab perusahaan</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Dokumen kapal meliputi Grosse Akta dan/atau Surat Ukur</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>BKP bagi kapal yang telah terdaftar sebagai kapal perikanan</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>SIPI/SIKPI bagi kapal yang telah memiliki SIPI/SIKPI</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Surat keterangan modifikasi dari galangan kapal/tukang pembuat (diketahui oleh Lurah dan/atau Camat setempat) yang menyatakan kapal dalam proses/selesai modifikasi dan memuat informasi sekurang-kurangnya nama pemilik kapal, lokasi dan waktu memulai modifikasi, rencana waktu penyelesaian, dan rincian modifikasi yang dilakukan</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Surat persetujuan penggunaan nama kapal atau persetujuan penggantian nama kapal dari Direktorat Jendral Perhubungan Laut, Kementrian Perhubungan, apabila ada penggantian nama kapal</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="border: 1px solid black" id="">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <span class="f12"><strong>Dokumentasi kapal berupa foto kapal kondisi terkini tampak haluan, samping, dan buritan dengan ukuran minimal 4R</strong></span>
                </div>
                <div class="col-md-8 text-end">
                    <button class="btn btn-primary-light btn-sm" type="button"><i class="fa-solid fa-plus"></i> Tambah</button>
                    <button class="btn btn-warning btn-sm" type="button"><i class="fa-solid fa-cloud-arrow-up"></i></i> Upload</button>
                    <button class="btn btn-info btn-sm" type="button"><i class="fa-solid fa-xmark"></i></i> Batal</button>
                    <button class="btn btn-danger btn-sm" type="button"><i class="fa-solid fa-trash"></i></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="bg-secondary">
                    <th>Dokumen</th>
                    <th>Tanggal Upload</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>