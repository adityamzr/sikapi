<div class="form-card">
    <div class="row">
        <div class="col-7">
            <h2 class="fs-title">Baru - arta mina</h2>
        </div>
        <div class="col-5">
            <h2 class="steps">Step 4 - 6</h2>
        </div>
    </div>
    <div class="mb-4 row">
        <div class="col-md-5 col-sm-12">
            <span class="f12">*Pastikan data telah terisi dan klik simpan sebelum cetak form 1 dan form 2</span>
        </div>
        <div class="col-md-7 col-sm-12 text-end">
            <a href="/permohonan/form/cetakForm1" class="btn btn-warning" type="button" id="cetakForm1" name="form1"><i class="fa-solid fa-print"></i> Cetak Form 1</a>
            <a href="/permohonan/form/cetakForm2" class="btn btn-warning" type="button" id="cetakForm2" name="form2"><i class="fa-solid fa-print"></i> Cetak Form 2</a>
            <button class="btn btn-secondary" name="simpan" id="simpan" type="button">Simpan</button>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Jenis Permohonan*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="" value="{{ old('jenis_permohonan', @$permohonan->jenis_permohonan) }}" id="jenis_permohonan" readonly>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nama Kapal*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="" value="{{ old('jenis_permohonan', @$permohonan->detail_permohonan->kapal->nama_kapal) }}" id="kapal" readonly>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Jenis Alat Tangkap*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="jenis_alat_tangkap" id="alat_tangkap" value="{{ old('jenis_alat_tangkap', @$permohonan->detail_permohonan->jenis_alat_tangkap) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    {{-- <div class="mb-2 row text-end" id="hidden_input1">
        <label for="" class="col-md-3 col-sm-6 col-form-label">WPP</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="wpp" id="wpp" >
        </div>
    </div> --}}
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Ukuran Kapal*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="ukuran_kapal" id="ukuran_kapal" value="{{ old('ukuran_kapal', @$permohonan->detail_permohonan->kapal->ukuran_kapal) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    {{-- <div class="mb-2 row text-end" id="hidden_input2">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tanda Pengenal Kapal BKP</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="bkp" id="bkp" >
        </div>
    </div> --}}
    {{-- <div class="mb-2 row text-end" id="hidden_input4">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nama Kapal Sebelumnya</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="nama_kapal_sebelumnya" id="nama_kapal_sebelumnya">
        </div>
    </div> --}}
    {{-- <div class="mb-2 row text-end" id="hidden_input5">
        <label for="" class="col-md-3 col-sm-6 col-form-label">ID Transmitter</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="id_transmiter" id="id_transmiter" >
        </div>
    </div> --}}
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nama Galangan*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="nama_galangan" id="nama_galangan" value="{{ old('nama_galangan', @$permohonan->detail_permohonan->galangan->nama_galangan) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Provinsi Galangan</label>
        <div class="col-md-3 col-sm-6 mb-2 mt-2">
            <select class="form-select" id="provinsi" name="provinsi_galangan" required>
                <option value="">--Pilih--</option>
                <option value="provinsi1" {{ old('provinsi_galangan', @$permohonan->detail_permohonan->galangan->provinsi_galangan) == 'provinsi1' ? 'selected' : '' }}>Provinsi1</option>
                <option value="provinsi2" {{ old('provinsi_galangan', @$permohonan->detail_permohonan->galangan->provinsi_galangan) == 'provinsi2' ? 'selected' : '' }}>Provinsi2</option>
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Kab/Kota Galangan</label>
        <div class="col-md-3 col-sm-6">
            <select class="form-select" id="kako" name="kabkota_galangan" required>
                <option value="">--Pilih--</option>
                <option value="kabkota1" {{ old('kabkota_galangan', @$permohonan->detail_permohonan->galangan->kabkota_galangan) == 'kabkota1' ? 'selected' : '' }}>Kota1</option>
                <option value="kabkota2" {{ old('kabkota_galangan', @$permohonan->detail_permohonan->galangan->kabkota_galangan) == 'kabkota2' ? 'selected' : '' }}>Kota2</option>
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Kecamatan Galangan</label>
        <div class="col-md-3 col-sm-6 mb-2 mt-2">
            <select class="form-select" id="kecamatan" name="kecamatan_galangan" required>
                <option value="">--Pilih--</option>
                <option value="kecamatan1" {{ old('kecamatan_galangan', @$permohonan->detail_permohonan->galangan->kecamatan_galangan) == 'kecamatan1' ? 'selected' : '' }}>Kecamatan1</option>
                <option value="kecamatan2" {{ old('kecamatan_galangan', @$permohonan->detail_permohonan->galangan->kecamatan_galangan) == 'kecamatan2' ? 'selected' : '' }}>Kecamatan2</option>
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Kelurahan Galangan</label>
        <div class="col-md-3 col-sm-6">
            <select class="form-select" id="kelurahan" name="kelurahan_galangan" required>
                <option value="">--Pilih--</option>
                <option value="kelurahan1" {{ old('kelurahan_galangan', @$permohonan->detail_permohonan->galangan->kelurahan_galangan) == 'kelurahan1' ? 'selected' : '' }}>Kelurahan1</option>
                <option value="kelurahan2" {{ old('kelurahan_galangan', @$permohonan->detail_permohonan->galangan->kelurahan_galangan) == 'kelurahan2' ? 'selected' : '' }}>Kelurahan2</option>
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Email Galangan</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="email_galangan" id="email_galangan" value="{{ old('email_galangan', @$permohonan->detail_permohonan->galangan->email_galangan) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Telepon Galangan</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="telepon_galangan" id="telepon_galangan" value="{{ old('telepon_galangan', @$permohonan->detail_permohonan->galangan->telepon_galangan) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">RT/RW Galangan</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="rtrw_galangan" id="rtrw"  value="{{ old('rtrw_galangan', @$permohonan->detail_permohonan->galangan->rtrw_galangan) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">KTP Perusahaan/Direktur Galangan</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="ktp_galangan" id="ktp" value="{{ old('ktp_galangan', @$permohonan->detail_permohonan->galangan->ktp_galangan) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Koordinat Latitude</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="lati" id="lati" value="{{ old('lati', @$permohonan->detail_permohonan->galangan->lati) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Koordinat Longitude</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="longi" id="longi" value="{{ old('longi', @$permohonan->detail_permohonan->galangan->longi) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="col-md-11 offset-1 mt-2 mb-3cd D:">
        <span>*Jika anda tidak tahu Koordinat Latitude dan Longitude daerah galangan anda, buka link berikut: </span><a href="https://www.latlong.net/" target="_blank">https://www.latlong.net/</a>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tempat Pembangunan</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="tempat_pembangunan" id="tempat_pembangunan" value="{{ old('tempat_pembangunan', @$permohonan->detail_permohonan->kapal->tempat_pembangunan) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tahun Pembangunan</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="tahun_pembangunan" id="tahun_pembangunan" value="{{ old('tahun_pembangunan', @$permohonan->detail_permohonan->kapal->tahun_pembangunan) }}" required> 
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Bahan Kapal</label>
        <div class="col-md-3 col-sm-6">
            <select class="form-select" id="bahan_kapal" name="bahan_kapal" required>
                <option value="">--Pilih--</option>
                <option value="Kayu" {{ old('bahan_kapal', @$permohonan->detail_permohonan->kapal->bahan_kapal) == 'Kayu' ? 'selected' : '' }}>Kayu</option>
                <option value="Besi" {{ old('bahan_kapal', @$permohonan->detail_permohonan->kapal->bahan_kapal) == 'Besi' ? 'selected' : '' }}>Besi</option>
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Range GT (alokasi SIUP)</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="range_gt" id="range_gt" value="{{ old('range_gt', @$permohonan->detail_permohonan->kapal->range_gt) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">GT kapal</label>
        <div class="col-md-2 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="gt_kapal" id="gt" value="{{ old('gt_kapal', @$permohonan->detail_permohonan->kapal->gt_kapal) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-2 col-sm-6 col-form-label">NT Kapal</label>
        <div class="col-md-2 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="nt_kapal" id="nt" value="{{ old('nt_kapal', @$permohonan->detail_permohonan->kapal->nt_kapal) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-1 col-sm-6 col-form-label">LoA</label>
        <div class="col-md-2 col-sm-6">
            <div class="input-group">
                <input class="form-control form-control-sm" type="number" step="0.001" id="my_number_field" name="loa" id="loa" value="{{ old('loa', @$permohonan->detail_permohonan->kapal->loa) }}" required>
                <div class="invalid-feedback">
                    Field harus diisi!
                </div>
                {{-- <span class="input-group-text">Meter</span> --}}
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Panjang</label>
        <div class="col-md-2 col-sm-6">
            <input class="form-control form-control-sm" type="number" step="0.001" name="panjang" id="panjang" value="{{ old('panjang', @$permohonan->detail_permohonan->kapal->panjang) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-2 col-sm-6 col-form-label">Lebar</label>
        <div class="col-md-2 col-sm-6">
            <input class="form-control form-control-sm" type="number" step="0.001" name="lebar" id="lebar" value="{{ old('lebar', @$permohonan->detail_permohonan->kapal->lebar) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-1 col-sm-6 col-form-label">Dalam</label>
        <div class="col-md-2 col-sm-6">
            <div class="input-group">
                <input class="form-control form-control-sm" type="number" step="0.001" name="dalam" id="dalam" value="{{ old('dalam', @$permohonan->detail_permohonan->kapal->dalam) }}" required>
                <div class="invalid-feedback">
                    Field harus diisi!
                </div>
                {{-- <span class="input-group-text">Meter</span> --}}
            </div>
        </div>
    </div>
    <div class="row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Merk Mesin</label>
        <div class="col-md-3 col-sm-6 mb-2 mt-2">
            <select class="form-select" id="merk_mesin" name="id_merk_mesin" required>
                <option value="">--Pilih--</option>
                @foreach ($merk_mesin as $merk)
                    <option value="{{ $merk->id_merk_mesin }}" {{ old('id_merk_mesin', @$permohonan->detail_permohonan->mesin->id_merk_mesin) == $merk->id_merk_mesin ? 'selected' : '' }}>{{ $merk->nama_merk_mesin }}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tipe Mesin</label>
        <div class="col-md-3 col-sm-6">
            <select class="form-select" id="tipe_mesin" name="id_tipe_mesin" required>
                <option value="">--Pilih--</option>
                @foreach ($tipe_mesin as $tipe)
                    <option value="{{ $tipe->id_tipe_mesin }}" {{ old('id_tipe_mesin', @$permohonan->detail_permohonan->mesin->id_tipe_mesin) == $tipe->id_tipe_mesin ? 'selected' : '' }}>{{ $tipe->nama_tipe_mesin }}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">No. Mesin</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="nomor_mesin" id="nomor_mesin" value="{{ old('nomor_mesin', @$permohonan->detail_permohonan->mesin->nomor_mesin) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Daya Mesin</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="daya_mesin" id="daya_mesin" value="{{ old('daya_mesin', @$permohonan->detail_permohonan->mesin->daya_mesin) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Jumlah Palka</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="jumlah_palka" id="jumlah_palka" value="{{ old('jumlah_palka', @$permohonan->detail_permohonan->kapal->jumlah_palka) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Kapasitas Palka</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="kapasitas_palka" id="kapasitas_palka" value="{{ old('kapasitas_palka', @$permohonan->detail_permohonan->kapal->kapasitas_palka) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Temperature Palka</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="temperatur_palka" id="temperatur_palka" value="{{ old('temperatur_palka', @$permohonan->detail_permohonan->kapal->temperatur_palka) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Pendingin Palka</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="pendingin_palka" id="pendingin_palka" value="{{ old('pendingin_palka', @$permohonan->detail_permohonan->kapal->pendingin_palka) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tgl. Pembangunan/Modifikasi</label>
        <div class="col-md-3 col-sm-6">
            <input class="datepicker-here form-control form-control-sm digits" type="text" data-language="en" name="tgl_pembangunan_dari" id="dari" value="{{ old('tgl_pembangunan_dari', @$dari) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Sampai dengan</label>
        <div class="col-md-3 col-sm-6">
            <input class="datepicker-here form-control form-control-sm digits" type="text" data-language="en" name="tgl_pembangunan_sampai" id="sampai" value="{{ old('tgl_pembangunan_sampai', @$sampai) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    {{-- <div class="card mt-4" id="hidden_div">
        <div class="card-header bg-warning py-2">
            <span class="f13"><strong>Rincian Perubahan</strong></span>
        </div>
        <div class="card-body pt-3" style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);">
            <table class="table">
                <thead class="table-secondary">
                    <th>Data Perubahan</th>
                    <th>Semula</th>
                    <th>Menjadi</th>
                    <th>
                        <button class="btn btn-dark btn-sm"><i class="fa-solid fa-plus"></i> Tambah</button>
                    </th>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td class="text-center"><i class="fa-solid fa-trash"></i></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> --}}
</div>