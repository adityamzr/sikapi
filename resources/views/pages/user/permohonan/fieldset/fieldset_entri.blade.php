<div class="form-card">
    <div class="row">
        <div class="col-7">
            <h4 class="fs-title">Entri Data Pemohon</h4>
        </div>
        <div class="col-5">
            <h2 class="steps">Step 2 - 6</h2>
        </div>
    </div> 
    <div class="mb-2 mt-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nomor Permohonan*</label>
        <div class="col-md-3 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="nomor_permohonan" id="nomor_permohonan" value="{{ old('nomor_permohonan', @$permohonan->nomor_permohonan) }}" readonly>
        </div>
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tgl. Permohonan*</label>
        <div class="col-md-3 col-sm-6">
            <input class="datepicker-here form-control form-control-sm digits" type="text" data-language="en" name="tanggal_permohonan" id="tanggal_permohonan" value="{{ old('tanggal_permohonan', @$tanggal) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Pemilik*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" value="{{ old('nama_pemilik', @$permohonan->detail_permohonan->siup->nama_pemilik) }}"  type="text" name="nama_pemilik" id="nama_pemilik" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nomor SIUP*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm " type="number" name="nomor_siup" id="nomor_siup" value="{{ old('nama_siup', @$permohonan->detail_permohonan->siup->nomor_siup) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Alamat Pemilik*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm " type="text" name="alamat_pemilik" id="alamat_pemilik" value="{{ old('alamat_pemilik', @$permohonan->detail_permohonan->siup->alamat_pemilik) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="kriteria" class="col-md-3 col-sm-6 col-form-label">Email Pemilik*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="email_pemilik" id="email_pemilik" value="{{ old('email_pemilik', @$permohonan->detail_permohonan->siup->email_pemilik) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Telepon Pemilik*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="number" name="telepon_pemilik" id="telepon_pemilik" value="{{ old('telepon_pemilik', @$permohonan->detail_permohonan->siup->telepon_pemilik) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
</div> 
