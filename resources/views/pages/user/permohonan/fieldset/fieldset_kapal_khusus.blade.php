
    <div class="row">
        <div class="col-7">
            <h2 class="fs-title">Tambah Permohonan Kapal</h2>
        </div>
        <div class="col-5">
            <h2 class="steps">Step 3 - 6</h2>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Jenis Permohonan</label>
        <div class="col-md-9 col-sm-6 mb-2">
            <select class="form-select" id="jenis" name="jenis_permohonan">
                <option value="">--Pilih--</option>
                <option value="stdh">Sudah Terbit Dokumen Hubla, Belum BKP</option>
                <option value="btdh">Belum Terbit Dokumen Hubla</option>
                <option value="spm">Sudah/Proses Modifikasi</option>
            </select>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Alokasi Izin Usaha</label>
        <div class="col-md-9 col-sm-6 mb-2">
            <select class="form-select" id="" name="alokasi_izin_usaha">
                <option value="">--Pilih--</option>
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
            </select>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tanda Pendaftaran Kapal<br><span class="f12">(Dokumen Perhubungan Laut)</span></label>
        <div class="col-md-9 col-sm-6 d-flex align-items-center">
            <input class="form-control form-control-sm my-0" id="formFileSm" type="file" name="dokumen_persyaratan">
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nama Kapal*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="nama_kapal" id="">
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tanda Selar</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="tanda_selar" id="">
        </div>
    </div>