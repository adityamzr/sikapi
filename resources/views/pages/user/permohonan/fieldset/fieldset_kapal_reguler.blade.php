<div class="form-card">
    <div class="row">
        <div class="col-7">
            <h2 class="fs-title">Tambah Permohonan Kapal</h2>
        </div>
        <div class="col-5">
            <h2 class="steps">Step 3 - 6</h2>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Jenis Permohonan</label>
        <div class="col-md-9 col-sm-6 mb-2">
            <select class="form-select jenis" id="select" name="jenis_permohonan" required>
                <option value="">--Pilih--</option>
                <option value="Pembangunan">Pembangunan</option>
                {{-- <option value="modif">Modifikasi</option> --}}
            </select>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Alokasi Izin Usaha</label>
        <div class="col-md-9 col-sm-6 mb-2">
            <select class="form-select" id="alokasi_izin_usaha" name="alokasi_izin_usaha" required>
                <option value="">--Pilih--</option>
                <option value="izin1" {{ old('alokasi_izin_usaha', @$permohonan->detail_permohonan->alokasi_izin_usaha) == 'izin1' ? 'selected' : '' }}>Izin usaha 1</option>
                <option value="izin2" {{ old('alokasi_izin_usaha', @$permohonan->detail_permohonan->alokasi_izin_usaha) == 'izin2' ? 'selected' : '' }}>izin usaha 2</option>
                <option value="izin3" {{ old('alokasi_izin_usaha', @$permohonan->detail_permohonan->alokasi_izin_usaha) == 'izin3' ? 'selected' : '' }}>izin usaha 3</option>
            </select>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tanda Pendaftaran Kapal<br><span class="f12">(Dokumen Perhubungan Laut)</span></label>
        <div class="col-md-9 col-sm-6 d-flex align-items-center">
            <input class="form-control form-control-sm my-0" id="formFileSm" type="file" name="file_tanda_pendaftaran_kapal" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Nama Kapal*</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="nama_kapal" id="nama_kapal" value="{{ old('nama_kapal', @$permohonan->detail_permohonan->kapal->nama_kapal) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
    <div class="mb-2 row text-end">
        <label for="" class="col-md-3 col-sm-6 col-form-label">Tanda Selar</label>
        <div class="col-md-9 col-sm-6">
            <input class="form-control form-control-sm" type="text" name="tanda_selar" id="tanda_selar" value="{{ old('tanda_selar', @$permohonan->detail_permohonan->kapal->tanda_selar) }}" required>
            <div class="invalid-feedback">
                Field harus diisi!
            </div>
        </div>
    </div>
</div>