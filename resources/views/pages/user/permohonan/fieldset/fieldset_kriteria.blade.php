<div class="form-card">
    <div class="row">
        <div class="col-7">
            <h4 class="fs-title">Tambah Kriteria Permohonan:</h4>
        </div>
        <div class="col-5">
            <h2 class="steps">Step 1 - 6</h2>
        </div>
    </div> 
    <div class="mb-4 row">
        <label for="" class="col-sm-3 col-form-label">Kriteria Permohonan</label>
        <div class="col-sm-9">
            <select class="form-select" id="kriteria" name="kriteria_permohonan">
                <option value="">--Pilih--</option>
                <option value="Reguler">Reguler</option>
                {{-- <option value="khusus">Khusus</option> --}}
            </select>
        </div>
    </div>
    <div class="mb-2">
        <span class="mb-4 f16">KETERANGAN:</span>
        <div class="row f12">
            <div class="col-lg-3">
                <strong>REGULER</strong>
            </div>
            <div class="col-lg-9">
                Kapal perikanan dalam rencan pembangunan dan belum dilakukan peletakan lunas (keel laying); atau kapal perikanan dalam rencana modifikasi dan belum dilakukan modifikasi
            </div>
        </div>
        <hr>
        <div class="row f12 mb-4">
            <div class="col-lg-3">
                <strong>KHUSUS</strong>
            </div>
            <div class="col-lg-9">
                Kapal perikanan dengan kondisi dalam proses/telah selesai dibangun atau proses/telah selesai modifikasi
            </div>
        </div>
        <div class="row f12 mt-2">
            <div class="col-lg-3 px-4">
                <strong>>>Sudah Bangun, Sudah Terbit Dokumen Hubla, belum memiliki BKP/SIPI/SIKPI</strong>
            </div>
            <div class="col-lg-9">
                Kapal sudah selesai pembangunan dan telah memiliki dokumen yang diterbitkan oleh Direktorat Jendral Perhubungan Laut, Kementrian Perhubungan, namun belum memiliki Buku Kapal Perikanan atau SIPI/SIKPI
            </div>
        </div>
        <div class="row f12 mt-4">
            <div class="col-lg-3 px-4">
                <strong>>>Sudah Bangun, Belum Terbit Dokumen Hubla</strong>
            </div>
            <div class="col-lg-9">
                Kapal dalam proses pembangunan atau selesai pembangunan dan belum memiliki dokumen yang diterbitkan oleh Direktorat Jendral Perhubungan Laut, Kementrian Perhubungan
            </div>
        </div>
        <div class="row f12 mt-4">
            <div class="col-lg-3 px-4">
                <strong>>>Sudah Proses Modifikasi</strong>
            </div>
            <div class="col-lg-9">
                Kapal dalam proses modifikasi atau selesai modifikasi
            </div>
        </div>
        <hr>
    </div>
</div>