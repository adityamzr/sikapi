<div class="form-card">
    <div class="row">
        <div class="col-7">
            <h2 class="fs-title">Persetujuan Permohonan</h2>
        </div>
        <div class="col-5">
            <h2 class="steps">Step 6 - 6</h2>
        </div>
    </div>
    <div class="col-12">
        <div class="card" style="border: 1px solid black">
            <div class="card-body">
                <p>1. Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium doloremque est tenetur repellendus culpa temporibus tempora asperiores autem. Ipsam, possimus!</p>
                <p>2. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quia, quae?</p>
                <p>3. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perferendis nemo dignissimos facere impedit eveniet molestiae.</p>
                <p>4. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Mollitia minima placeat non illum. Nesciunt voluptas natus maxime velit ipsa numquam!</p>
            </div>
            <div class="mb-3 text-center">
                <div class="form-check">
                  <div class="checkbox p-0">
                    <input class="form-check-input" id="invalidCheck" type="checkbox" value="yes" required="">
                    <label class="form-check-label" for="invalidCheck">Setuju</label>
                  </div>
                  <div class="invalid-feedback">Centang Setuju untuk Submit</div>
                </div>
            </div>
        </div>
    </div>
</div>