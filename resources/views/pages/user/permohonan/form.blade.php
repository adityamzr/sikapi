@extends('layouts.body')

@section('content')
<div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
        @if (!empty($permohonan))
        <h3>Edit Permohonan</h3>  
        @else
          <h3>Tambah Permohonan</h3>
        @endif
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/permohonan">Permohonan</a></li>
            @if (!empty($permohonan))
                <li class="breadcrumb-item">Edit Permohonan</li>
            @else
                <li class="breadcrumb-item">Tambah Permohonan</li>
            @endif
            {{-- <li class="breadcrumb-item">Color Version</li>
            <li class="breadcrumb-item active">Layout Light</li> --}}
          </ol>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row starter-main">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-body">
                <form id="msform" action="{{ url('/permohonan', @$permohonan->id_permohonan) }}" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($permohonan))
                        @method('PATCH')
                    @endif
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="account"><strong class="f13">Kriteria</strong></li>
                        <li id="people-pen"><strong class="f13">Entri Permohonan</strong></li>
                        <li id="form-pen"><strong class="f13">Permohonan Kapal</strong></li>
                        <li id="print"><strong class="f13">Cetak Form</strong></li>
                        <li id="doc"><strong class="f13">Unggah Dokumen</strong></li>
                        <li id="confirm"><strong class="f13">Persetujuan</strong></li>
                    </ul>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    {{-- FIELDSET KRITERIA START --}}
                    <fieldset>
                        @include('pages.user.permohonan.fieldset.fieldset_kriteria')
                        <button type="button" name="next" id="next_kriteria" class="btn btn-primary btn-sm next action-button">Next</button>
                    </fieldset>
                    {{-- FIELDSET KRITERIA END --}}
                    {{-- FIELDSET ENTRI PERMOHONAN START --}}
                    <fieldset id="fieldset-entri">
                        @include('pages.user.permohonan.fieldset.fieldset_entri')
                        <button type="button" name="next" id="next_entri" class="btn btn-primary btn-sm next action-button">Next</button> 
                        <button type="button" name="previous" class="btn btn-info btn-sm previous action-button-previous">Back</button>
                    </fieldset>
                    {{-- FIELDSET ENTRI PERMOHONAN END --}}
                    {{-- PERMOHONAN KAPAL START --}}
                    <fieldset>
                        <div class="form-card" id="div_reguler">
                            @include('pages.user.permohonan.fieldset.fieldset_kapal_reguler')
                        </div>
                        {{-- <div class="form-card" id="div_khusus">
                            @include('pages.user.permohonan.fieldset.fieldset_kapal_khusus')
                        </div> --}}
                        <button type="button" name="next" id="next_permohonan" class="btn btn-primary btn-sm next action-button">Next</button>
                        <button type="button" name="previous" class="btn btn-info btn-sm previous action-button-previous">Back</button>
                    </fieldset>
                    {{-- PERMOHONAN KAPAL END --}}
                    {{-- CETAK FORM START --}}
                    <fieldset>
                        {{-- <input type="text" id="kapal_hidden" class="kapal_hide"> --}}
                        @include('pages.user.permohonan.fieldset.fieldset_cetak')
                        <button type="button" id="next_cetak" name="next" class="btn btn-primary btn-sm next action-button next_cetak">Next</button>
                        <button type="button" name="previous" class="btn btn-info btn-sm previous action-button-previous">Back</button>
                    </fieldset>
                    {{-- CETAK FORM END --}}
                    {{-- UNGGAH DOKUMEN START --}}
                    <fieldset>
                        <div class="form-card" id="div_baru">
                            @include('pages.user.permohonan.fieldset.dokumen.dok_baru')
                        </div>
                        {{-- <div class="form-card" id="div_stdh">
                            @include('pages.user.permohonan.fieldset.dokumen.dok_stdh')
                        </div>
                        <div class="form-card" id="div_btdh">
                            @include('pages.user.permohonan.fieldset.dokumen.dok_btdh')
                        </div>
                        <div class="form-card" id="div_spm">
                            @include('pages.user.permohonan.fieldset.dokumen.dok_spm')
                        </div> --}}
                        <button type="button" name="next" class="btn btn-primary btn-sm next action-button">Next</button>
                        <button type="button" name="previous" class="btn btn-info btn-sm previous action-button-previous">Back</button>
                    </fieldset>
                    {{-- UNGGAH DOKUMEN END --}}
                {{-- SELESAI START--}}
                    <fieldset>
                        @include('pages.user.permohonan.fieldset.fieldset_persetujuan')
                        <input type="hidden" name="id_user" value="{{ auth()->user()->id_user }}">
                        <button type="submit" id="submit_permohonan" class="btn btn-primary btn-sm next action-button" disabled>Submit</button>
                        <button type="button" name="previous" class="btn btn-info btn-sm previous action-button-previous">Back</button>
                    </fieldset>
                   {{-- SELESAI END  --}}
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>  
</div>
@endsection
@push('script')
<script>
    let base_url = "{{ url('') }}";
</script>
<script src="{{ asset('assets') }}/js/custom.js"></script>
<script src="{{ asset('assets') }}/js/custom2.js"></script>
@endpush