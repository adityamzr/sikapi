<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\User\PermohonanController;
use App\Http\Controllers\Admin\PermohonanBaruController;
use App\Http\Controllers\Admin\StatusController;
use App\Http\Controllers\Admin\PermohonanSTDHController;
use App\Http\Controllers\Admin\PermohonanBTDHController;
use App\Http\Controllers\Admin\PermohonanSPMController;
use App\Http\Controllers\Admin\MerkMesinController;
use App\Http\Controllers\Admin\TipeMesinController;
use App\Http\Controllers\PetaController;
use App\Models\User;
use Monolog\Handler\RotatingFileHandler;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [LoginController::class, 'index'])->name('login');
Route::post('/', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'index']);

Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);

Route::middleware('auth')->group(function(){    
    Route::get('/beranda', [BerandaController::class, 'index']);
    Route::post('/beranda', [BerandaController::class, 'store']);
// ====================== PERMOHONAN USER ======================
    Route::get('/permohonan', [PermohonanController::class, 'index']);
    Route::post('permohonan/getData', [PermohonanController::class, 'getAllData'])->name('permohonan.get');
    Route::get('permohonan/form', [PermohonanController::class, 'create']);
    Route::post('permohonan', [PermohonanController::class, 'store']);
    Route::get('/permohonan/{id}/edit', [PermohonanController::class, 'edit']);
    Route::patch('/permohonan/{id}', [PermohonanController::class, 'update']);
    Route::post('/permohonan/{id}', [PermohonanController::class, 'destroy'])->name('hapus_permohonan');
    
    Route::get('permohonan/form/cetakForm1', [PermohonanController::class, 'cetakForm1']);
    Route::get('permohonan/form/cetakForm2', [PermohonanController::class, 'cetakForm2']);
    Route::get('permohonan/{id}/cetak_sk', [PermohonanController::class, 'cetakSK']);
    // ====================== PERMOHONAN ADMIN ======================
    Route::get('permohonan/baru', [PermohonanBaruController::class, 'index']);
    Route::post('permohonan/baru/getData', [PermohonanBaruController::class, 'getAllData'])->name('permohonanBaru.get');
    Route::get('/permohonan/baru/{id}/edit', [PermohonanBaruController::class, 'edit']);
    Route::patch('/permohonan/baru/{id}', [PermohonanBaruController::class, 'update']);
    Route::post('/permohonan/baru/{id}', [PermohonanBaruController::class, 'destroy']);
    
    // Route::get('permohonan/stdh', [PermohonanSTDHController::class, 'index']);
    
    // Route::get('permohonan/btdh', [PermohonanBTDHController::class, 'index']);
    
    // Route::get('permohonan/spm', [PermohonanSPMController::class, 'index']);
    
    Route::get('peta-galangan', [PetaController::class, 'index']);
    
    Route::get('merk-mesin', [MerkMesinController::class, 'index']);
    Route::post('merk-mesin/getData', [MerkMesinController::class, 'getAllData'])->name('merk_mesin.get');
    Route::get('merk-mesin/form', [MerkMesinController::class, 'create']);
    Route::post('merk-mesin', [MerkMesinController::class, 'store']);
    Route::get('merk-mesin/{id}/edit', [MerkMesinController::class, 'edit']);
    Route::patch('merk-mesin/{id}', [MerkMesinController::class, 'update']);
    Route::post('merk-mesin/{id}', [MerkMesinController::class, 'destroy']);
    
    Route::get('tipe-mesin', [TipeMesinController::class, 'index']);
    Route::post('tipe-mesin/getData', [TipeMesinController::class, 'getAllData'])->name('tipe_mesin.get');
    Route::get('tipe-mesin/form', [TipeMesinController::class, 'create']);
    Route::post('tipe-mesin', [TipeMesinController::class, 'store']);
    Route::get('tipe-mesin/{id}/edit', [TipeMesinController::class, 'edit']);
    Route::patch('tipe-mesin/{id}', [TipeMesinController::class, 'update']);
    Route::post('tipe-mesin/{id}', [TipeMesinController::class, 'destroy']);
    
    Route::get('status', [StatusController::class, 'index']);
    Route::post('status/getData', [StatusController::class, 'getAllData'])->name('status.get');
    Route::get('status/form', [StatusController::class, 'create']);
    Route::post('status', [StatusController::class, 'store']);
    Route::get('status/{id}/edit', [StatusController::class, 'edit']);
    Route::patch('status/{id}', [StatusController::class, 'update']);
    Route::post('status/{id}', [StatusController::class, 'destroy']);
});
    
    

// Route::get('/permohonan/modifikasi', [PermohonanModifController::class, 'index']);